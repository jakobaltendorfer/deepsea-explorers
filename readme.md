# Deep Sea Explorers

Deep Sea Explorers is a cooperative and explorative 3D game that takes three players on a journey into the ocean aboard an advanced submarine. As the game progresses the players have to work together as a team to steer the submarine, sabotage corporate operations, evade and destroy underwater threats and collect evidence. The three-player co-op design promotes teamwork, strategic discussions, and a shared gameplay experience, ensuring that players remain hooked until the very end. 

## Team Members

- Jakob Altendorfer, 11812035
- Florian Gamillschegg, 11816353
- Emma Haidacher, 11818854
- Lukas Ropele, 11825728

## Technologies Used

- We used **Unity 2023.1** as the game engine
- For multiplayer functionality we used the following:
  - **Unity Netcode for GameObjects (NGO)** 
  - **Unity Lobby Service** 
  - **Unity Relay** 
- **Unity DOTS** was used for simulating a large number of boids
- **Blender** as used for modelling
- **Affinity Photo** was used for sprite creation

## Credits

- **Sounds**: https://pixabay.com/
- **Assets**:
  - https://sketchfab.com/MRowa/collections/buildings-a54043588c7c4826919cafa05a6addc1
  - https://sketchfab.com/3d-models/rocket-turret-free-851d6d54020944608840951800085025
  - https://assetstore.unity.com/packages/3d/environments/fantasy/polygon-tropical-jungle-nature-biomes-low-poly-3d-art-by-synty-234253
  - https://assetstore.unity.com/packages/3d/environments/urban/polygon-battle-royale-low-poly-3d-art-by-synty-128513
  - https://assetstore.unity.com/packages/3d/environments/industrial/polygon-gang-warfare-low-poly-3d-art-by-synty-150589
 - **Shaders**: 
    - https://assetstore.unity.com/packages/vfx/shaders/stylized-grass-shader-143830
    - https://assetstore.unity.com/packages/vfx/shaders/fullscreen-camera-effects/buto-volumetric-fog-volumetric-lighting-height-fog-urp-2021-213497

The entire submarine as well as all the models inside were created by us.

## Game Build & Source Code

To play our game, check out the build on [itch.io](https://knopele.itch.io/deepsea-explorers)

