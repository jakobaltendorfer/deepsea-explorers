using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndingScript : MonoBehaviour
{
    [SerializeField] private float fadeDuration = 2.0f;
    [SerializeField] private float _letterDisplayDelay = 0.5f;

    [SerializeField] private string endingMessage = "The End";
    [SerializeField] private GameObject quitButton;

    private Color _startColor = new Color(0, 0, 0, 0);
    private Image _blackScreen;
    private float _elapsedTimeFade = 0.0f;
    private Color _targetColor;
    private TMP_Text _tMP;
    private bool _finishedFading = false;
    private bool _messageDisplayed = false;
    private int _currentLetterIndex = 0;
    private float _letterDisplayTimer = 0.0f;

    void Start()
    {
        // image
        _blackScreen = GetComponentInChildren<Image>();
        _blackScreen.color = new Color(0, 0, 0, 0);
        _targetColor = new Color(0, 0, 0, 1);

        // text
        _tMP = GetComponentInChildren<TMP_Text>();
        _tMP.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        _blackScreen.color = Color.Lerp(_startColor, _targetColor, _elapsedTimeFade / fadeDuration);
        _elapsedTimeFade += Time.deltaTime;
        if (!_finishedFading)
        {
            if (_elapsedTimeFade < fadeDuration)
            {
                _blackScreen.color = Color.Lerp(_startColor, _targetColor, _elapsedTimeFade / fadeDuration);
                _elapsedTimeFade += Time.deltaTime;
            }
            else
            {
                _finishedFading = true;
            }
        }

        if (_elapsedTimeFade > fadeDuration && _currentLetterIndex < endingMessage.Length)
        {
            _letterDisplayTimer += Time.deltaTime;

            if (_letterDisplayTimer >= _letterDisplayDelay)
            {
                _tMP.text += endingMessage[_currentLetterIndex];
                _currentLetterIndex++;
                _letterDisplayTimer = 0.0f;

                if (_currentLetterIndex >= endingMessage.Length)
                {
                    quitButton.SetActive(true);
                    Cursor.visible = true;
                }
            }
        }
    }
}