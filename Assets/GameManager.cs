using System;
using System.Collections;
using System.Collections.Generic;
using Networking;
using Submarine;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : NetworkBehaviour
{
    public static GameManager Instance { get; private set; }
    
    [SerializeField] private GameObject player;
    private int _spawnIndex = 0;
    private int _numPlayersFinishedLoading = 0;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        CustomNetworkManager.Instance.OnClientConnected += OnClientConnected;
    }

    private void OnClientConnected(object sender, CustomNetworkManager.OnClientConnectedEventArgs e)
    {
        if (SceneManager.GetActiveScene().name == "SampleScene")
        {
            DelayedInitializePlayers();
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "SampleScene")
        {
            FinishLoadingServerRpc();
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void FinishLoadingServerRpc()
    {
        Debug.Log("FinishLoadingServerRpc GameManager");

        _numPlayersFinishedLoading++;
        if (LobbyManager.Instance != null && _numPlayersFinishedLoading == LobbyManager.Instance.GetNumPlayers())
        {
            InitializePlayers();
        }
    }


    private void InitializePlayers()
    {
        if (!IsHost) return;
        Invoke(nameof(DelayedInitializePlayers), 4f);
    }

    private Vector3 GetSpawnPosition()
    {
        var spawnPoints = SpawnController.Instance.transform;
        Vector3 pos = spawnPoints.GetChild(_spawnIndex).position;


        _spawnIndex++;
        if (_spawnIndex >= spawnPoints.childCount)
            _spawnIndex = 0;

        return pos;
    }

    private void DelayedInitializePlayers()
    {
        Debug.Log("DelayedInitializePlayers GameManager");
        if (!IsHost) return;
        var submarine = SubmarineController.Instance.transform;

        foreach (var client in NetworkManager.ConnectedClientsList)
        {
            if (client.PlayerObject != null)
                continue;

            var position = GetSpawnPosition();
            GameObject go = Instantiate(player, position, Quaternion.identity);
            var networkObject = go.GetComponent<NetworkObject>();
            networkObject.SpawnAsPlayerObject(client.ClientId);
            networkObject.TrySetParent(submarine, true);

            var clientRpcParams = new ClientRpcParams
            {
                Send = new ClientRpcSendParams
                {
                    TargetClientIds = new ulong[] {client.ClientId}
                }
            };
            PlayIntroClientRpc(clientRpcParams);
        }
    }

    [ClientRpc]
    private void PlayIntroClientRpc(ClientRpcParams clientRpcParams = default)
    {
        DollyCamera.Instance.PlayIntro();
    }

    public void StartGame()
    {
        DialogueManager.Instance.StartDialogue("Start");
        SubmarineVisual.Instance.ActivateTopDown();
    }
}