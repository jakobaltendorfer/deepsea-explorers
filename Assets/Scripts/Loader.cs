using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Loader
{
    public enum Scene
    {
        Menu,
        SampleScene
    }

    private static TaskCompletionSource<bool> _sceneLoadTcs;
    
    public static void Load(Scene scene)
    {
        NetworkManager.Singleton.SceneManager.LoadScene(scene.ToString(), LoadSceneMode.Single);
    }


    public static Task LoadAsync(Scene scene)
    {
        if (_sceneLoadTcs != null && !_sceneLoadTcs.Task.IsCompleted)
        {
            throw new System.InvalidOperationException("An older scene load operation is still ongoing");
        }

        NetworkManager.Singleton.SceneManager.LoadScene(scene.ToString(), LoadSceneMode.Single);
        NetworkManager.Singleton.SceneManager.OnSceneEvent -= OnSceneEvent;
        NetworkManager.Singleton.SceneManager.OnSceneEvent += OnSceneEvent;
        
        return _sceneLoadTcs!.Task;
    }

    private static void OnSceneEvent(SceneEvent sceneEvent)
    {
        Debug.Log(sceneEvent);
        if (sceneEvent.SceneEventType == SceneEventType.LoadEventCompleted)
        {
            NetworkManager.Singleton.SceneManager.OnSceneEvent -= OnSceneEvent;
            _sceneLoadTcs.SetResult(true);
        }
    }
}