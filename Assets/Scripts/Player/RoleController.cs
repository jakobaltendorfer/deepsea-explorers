using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoleController : MonoBehaviour
{
    public enum Role
    {
        Captain = 0,
        Crewmember = 1,
    }

    public static Dictionary<Role, string> roleToText = new Dictionary<Role, string>()
    {
        {Role.Captain, "Captain"},
        {Role.Crewmember, "Crewmember"},
    };
}