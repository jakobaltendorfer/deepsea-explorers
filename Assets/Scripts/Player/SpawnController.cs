using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class SpawnController : NetworkBehaviour
{
    private NetworkVariable<int> _spawnIndex = new NetworkVariable<int>();

    public static SpawnController Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public Vector3 GetSpawnPosition()
    {
        int index = _spawnIndex.Value;
        Vector3 pos = transform.GetChild(index).position;

        _spawnIndex.Value++;
        if (_spawnIndex.Value >= transform.childCount)
            _spawnIndex.Value = 0;

        return pos;
    }
}