﻿using System;
using Submarine;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerMovement : NetworkBehaviour
{
    [SerializeField] private float playerRadius = 0.6f;
    [SerializeField] private float speed = 1.5f;
    [SerializeField] private float rotationSpeed = 1.5f;
    [SerializeField] private float playerHeight = 2f;
    [SerializeField] private bool debug;
    [SerializeField] private LayerMask collisionMask;

    private PlayerVisual _playerVisual;

    private void Start()
    {
        _playerVisual = GetComponentInChildren<PlayerVisual>();
    }

    void Update()
    {
        if (IsOwner)
            MovePlayer();
    }

    public void OnInteract()
    {
        _playerVisual.SetIsRunning(false);
    }


    private void MovePlayer()
    {
        if (GameInput.Instance == null) return;
        var inputVector = GameInput.Instance.GetMovementVector();

        Vector3 movement = new Vector3(inputVector.x, 0, inputVector.y);
        var submarine = SubmarineController.Instance.transform;
        movement = submarine.TransformDirection(movement);
        // var transformedUp = submarine.TransformDirection(Vector3.up);
        if (movement != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(movement, submarine.up);
            transform.rotation =
                Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }

        _playerVisual.SetIsRunning(movement != Vector3.zero);

        var pos = transform.position;
        float moveDistance = speed * Time.deltaTime;
        bool canMove = !Physics.CapsuleCast(pos, pos + submarine.up * playerHeight, playerRadius, movement,
            moveDistance, collisionMask);

        if (debug)
        {
            var position = transform.position + movement * moveDistance;
            var rotation = transform.rotation;

            DebugUtils.DrawCapsule(position, rotation, playerHeight, playerRadius, Color.blue);
        }

        if (!canMove)
        {
            movement = new Vector3(0, 0, inputVector.y);
            movement = submarine.TransformDirection(movement);
            canMove = !Physics.CapsuleCast(pos, pos + submarine.up * playerHeight, playerRadius, movement, moveDistance,
                collisionMask);

            if (!canMove)
            {
                movement = new Vector3(inputVector.x, 0, 0);
                movement = submarine.TransformDirection(movement);
                canMove = !Physics.CapsuleCast(pos, pos + submarine.up * playerHeight, playerRadius, movement,
                    moveDistance, collisionMask);
            }
        }


        if (canMove)
        {
            transform.Translate(movement * moveDistance, Space.World);
        }
    }
}