using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCanvas : MonoBehaviour
{
    public static PlayerCanvas Instance { get; private set; }
    [SerializeField] private GameObject pickUpAmmo;
    [SerializeField] private GameObject pickUpSpecialAmmo;
    [SerializeField] private GameObject sonarImage;
    [SerializeField] private GameObject crosshair;
    [SerializeField] private GameObject cameraBackground;
    [SerializeField] private GameObject photoFrame;
    [SerializeField] private Image photoFrameSprite;
    [SerializeField] private GameObject bulletCount;
    [SerializeField] private GameObject ammoBullet;
    [SerializeField] private GameObject ammoRocket;
    [SerializeField] private GameObject submarineHealthBar;
    [SerializeField] private GameObject submarineHealthBarFill;

    private Slider _slider;


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        _slider = bulletCount.GetComponentInChildren<Slider>();
    }

    public void EnablePickUpAmmo(AmmoBox.AmmoType ammoType, bool active)
    {
        if (ammoType == AmmoBox.AmmoType.Special)
        {
            pickUpSpecialAmmo.SetActive(active);
        }
        else
        {
            pickUpAmmo.SetActive(active);
        }
    }

    public void EnableSonar(bool active)
    {
        sonarImage.SetActive(active);
        submarineHealthBar.SetActive(active);
    }

    public void EnableCrosshair(bool active)
    {
        crosshair.SetActive(active);
        bulletCount.SetActive(active);
    }

    public void EnableCameraBackground(bool active)
    {
        cameraBackground.SetActive(active);
    }

    public void EnablePhotoFrame(bool active)
    {
        photoFrame.SetActive(active);
    }

    public void SetPhotoFrameSprite(Sprite sprite)
    {
        photoFrameSprite.sprite = sprite;
    }

    public void UpdateBulletCount(int count, int maxCount, bool isSpecialBullet)
    {
        _slider.value = count / (float) maxCount;
        ammoBullet.SetActive(!isSpecialBullet);
        ammoRocket.SetActive(isSpecialBullet);
    }

    public void UpdateHealthBar(int health, int maxHealth, bool critical)
    {
        submarineHealthBar.GetComponentInChildren<Slider>().value = health / (float) maxHealth;
        submarineHealthBarFill.GetComponent<Image>().color = critical ? Color.red : Color.green;
    }
}