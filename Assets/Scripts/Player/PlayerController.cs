using System.Collections.Generic;
using System.Linq;
using Networking;
using Submarine;
using Unity.Netcode;
using Unity.Services.Authentication;
using UnityEngine;


public class PlayerController : NetworkBehaviour
{
    [SerializeField] private NetworkVariable<RoleController.Role> role;
    public bool pickedAmmo { get; set; }
    public bool pickedSpecialAmmo { get; set; }
    [SerializeField] private float highlightMaxDistance = 1f;
    [SerializeField] private LayerMask highlightLayerMask;
    [SerializeField] private float highlightHeightOffset = 1f;

    public static PlayerController LocalPlayer { get; private set; }

    private HashSet<HighlightOnLook> _highlightedCollider = new HashSet<HighlightOnLook>();


    [SerializeField] private EnterModule currentModule;

    public EnterModule CurrentModule
    {
        get => currentModule;
        set => currentModule = value;
    }


    private void Start()
    {
        if (IsOwner)
        {
            LocalPlayer = this;
        }

        role.OnValueChanged += OnRoleChanged;
    }

    void OnRoleChanged(RoleController.Role old, RoleController.Role newRole)
    {
        ChangeRoleServerRpc(newRole);
    }

    public override void OnNetworkSpawn()
    {
        Debug.Log("Player " + OwnerClientId + " spawned.");

        CustomNetworkManager.Instance.OnPlayerSpawn(this);

        if (IsOwner && LobbyManager.Instance != null)
        {
            var players = LobbyManager.Instance.GetLobbyPlayers();
            var player = players.First(p => p.Id == AuthenticationService.Instance.PlayerId);
            var newRole = (RoleController.Role) int.Parse(player.Data[LobbyManager.PlayerRoleKey].Value);
            ChangeRoleServerRpc(newRole);
        }

        if (IsOwner)
        {
            LocalPlayer = this;
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void ChangeRoleServerRpc(RoleController.Role newRole)
    {
        role.Value = newRole;
        UpdateRoleClientRpc(newRole);
    }

    [ClientRpc]
    private void UpdateRoleClientRpc(RoleController.Role newRole)
    {
        var playerVisual = GetComponentInChildren<PlayerVisual>();
        playerVisual.ChangeRoleVisual(newRole);
    }

    public void IsInteracting()
    {
        GetComponent<PlayerMovement>().OnInteract();
    }

    public bool IsCaptain()
    {
        return role.Value == RoleController.Role.Captain;
    }


    void Update()
    {
        if (IsOwner)
            CheckHighlights();
    }

    // ReSharper disable Unity.PerformanceAnalysis
    private void CheckHighlights()
    {
        HashSet<HighlightOnLook> highlightedColliderThisFrame = new HashSet<HighlightOnLook>();
        Ray ray = new Ray(transform.position + transform.up * highlightHeightOffset, transform.forward);
        RaycastHit[] hits = Physics.RaycastAll(ray, highlightMaxDistance, highlightLayerMask);

        hits = hits.OrderBy(h => h.distance).ToArray();

        foreach (var hit in hits)
        {
            HighlightOnLook highlightScript = hit.collider.GetComponent<HighlightOnLook>();
            if (highlightScript != null)
            {
                if (!_highlightedCollider.Contains(highlightScript))
                {
                    highlightScript.OnLookAt(this);
                }

                highlightedColliderThisFrame.Add(highlightScript);
            }
        }

        foreach (var prevHighlighted in _highlightedCollider)
        {
            if (!highlightedColliderThisFrame.Contains(prevHighlighted))
            {
                prevHighlighted.OnLookAway();
            }
        }

        _highlightedCollider = highlightedColliderThisFrame;
    }
}