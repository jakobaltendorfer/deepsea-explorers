using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerVisual : MonoBehaviour
{
    [SerializeField] private Transform head;

    private Animator _animator;
    private static readonly int IsRunning = Animator.StringToHash("isRunning");
    private bool _isAnimatorNotNull;
    private bool _isSoundSourceNotNull;
    private RoleController.Role _role = RoleController.Role.Captain;
    private AudioSource _runningSound;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _runningSound = GetComponent<AudioSource>();
        _isAnimatorNotNull = _animator != null;
        _isSoundSourceNotNull = _runningSound != null;
    }

    public void ChangeRoleVisual(RoleController.Role role)
    {
        Debug.Log("ChangeRoleVisual " + role);
        _role = role;
        switch (role)
        {
            case RoleController.Role.Captain:
                head.gameObject.SetActive(true);
                break;
            case RoleController.Role.Crewmember:
                head.gameObject.SetActive(false);
                break;
        }
    }

    public void SetIsRunning(bool isRunning)
    {
        if (_isAnimatorNotNull)
        {
            _animator.SetBool(IsRunning, isRunning);
        }

        EnableRunningSoundServerRpc(isRunning);
    }

    [ServerRpc(RequireOwnership = false)]
    private void EnableRunningSoundServerRpc(bool enabled)
    {
        EnableRunningSoundClientRpc(enabled);
    }

    [ClientRpc]
    private void EnableRunningSoundClientRpc(bool enabled)
    {
        if (_isSoundSourceNotNull)
        {
            _runningSound.enabled = enabled;
        }
    }
}