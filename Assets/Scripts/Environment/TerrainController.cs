using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainController : MonoBehaviour
{
    public static TerrainController Instance { get; private set; }

    private Terrain _terrain;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    private void Start()
    {
        _terrain = GetComponent<Terrain>();
    }

    public float SampleTerrainHeight(float worldX, float worldZ)
    {
        if (_terrain == null) return 0f;

        TerrainData terrainData = _terrain.terrainData;

        // Convert world position to terrain local position
        Vector3 localPosition = _terrain.transform.InverseTransformPoint(new Vector3(worldX, 0, worldZ));

        // Normalize the local position
        float normalizedX = localPosition.x / terrainData.size.x;
        float normalizedZ = localPosition.z / terrainData.size.z;

        // Use the normalized position to get the height
        float height = terrainData.GetInterpolatedHeight(normalizedX, normalizedZ);

        return height + _terrain.transform.position.y;
    }
}