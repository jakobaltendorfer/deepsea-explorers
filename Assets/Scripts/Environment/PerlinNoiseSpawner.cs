using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Profiling;
using UnityEngine;
using Random = UnityEngine.Random;

public class PerlinNoiseSpawner : MonoBehaviour
{
    [Tooltip("Maximum 1024 because of GPU Instancing")] [SerializeField]
    private int instancesPerCell = 100;

    [SerializeField] private float cellSize = 10f;
    [SerializeField] private GameObject objectToSpawn;
    [SerializeField] private int cellRadius = 2;
    [SerializeField] private float noiseScale = 0.1f;
    [SerializeField] private float scaleMax = 1f;
    [SerializeField] private float power = 1f;
    [SerializeField] private float multiplier = 1f;
    [SerializeField] private float cellBoundsPadding = 5f;
    [SerializeField] private float randomScale = 1f;

    private Vector2Int _currentCell;
    private Vector2Int _oldCell;
    private Transform _cameraTransform;
    private RenderParams _renderParams;


    private Dictionary<Vector2Int, Matrix4x4[]> _cellMap = new Dictionary<Vector2Int, Matrix4x4[]>();
    private Dictionary<Vector2Int, List<float>> _cellRandomValuesMap = new Dictionary<Vector2Int, List<float>>();
    private List<Matrix4x4[]> _cellMatrices = new List<Matrix4x4[]>(); // Cache this list outside to prevent allocations
    private List<Vector2Int> _cellKeys = new List<Vector2Int>(); // Cache this list outside to prevent allocations
    private Vector3[] _offsets;

    private Mesh _instanceMesh;
    private Material _instanceMaterial;

    private Terrain _terrain;
    static readonly ProfilerMarker Marker = new ProfilerMarker("PerlinNoiseSpawner.RenderMeshes");
    static readonly ProfilerMarker Marker2 = new ProfilerMarker("PerlinNoiseSpawner.InnerLoop");
    static readonly ProfilerMarker Marker3 = new ProfilerMarker("PerlinNoiseSpawner.ComputeCellBoundingVolume");
    static readonly ProfilerMarker Marker4 = new ProfilerMarker("PerlinNoiseSpawner.RenderMeshInstanced");


    void Start()
    {
        _cameraTransform = Camera.main.transform;

        Random.InitState(0);

        _terrain = GetComponent<Terrain>();
        _instanceMesh = objectToSpawn.GetComponent<MeshFilter>().sharedMesh;
        _instanceMaterial = objectToSpawn.GetComponent<MeshRenderer>().sharedMaterial;
        instancesPerCell = Math.Min(instancesPerCell, 1024);

        IsInNewCell();
        InitializeCellMap();

        _cellMatrices = _cellMap.Values.ToList();
        _cellKeys = _cellMap.Keys.ToList();

        _renderParams = new RenderParams(_instanceMaterial)
        {
            receiveShadows = false,
            shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off
        };
    }

    void Update()
    {
        if (IsInNewCell())
        {
            CellCircleCalculation();
        }


        RenderMeshes();
    }


    void InitializeCellMap()
    {
        HashSet<Vector2Int> cellCircle = GetCellsWithinRadius(_currentCell);
        InitializeRandomOffsets();

        foreach (Vector2Int cell in cellCircle)
        {
            PopulateCell(cell);
        }
    }

    private void InitializeRandomOffsets()
    {
        _offsets = new Vector3[instancesPerCell];
        for (int i = 0; i < instancesPerCell; i++)
        {
            _offsets[i] = new Vector3(Random.Range(-randomScale, randomScale), 0,
                Random.Range(-randomScale, randomScale));
        }
    }

    private void PopulateCell(Vector2Int cell)
    {
        Matrix4x4[] matrices = new Matrix4x4[instancesPerCell];

        List<float> randoms = new List<float>();

        for (int i = 0; i < instancesPerCell; i++)
        {
            float scale = Random.Range(0f, scaleMax);
            Vector3 localScale = new Vector3(scale, scale, scale);
            Quaternion rotation = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);

            // Initially set to some off-screen position
            Vector3 position = new Vector3(-1000, -1000, -1000);

            matrices[i] = Matrix4x4.TRS(position, rotation, localScale);
            randoms.Add(Random.value);
        }

        _cellMap.Add(cell, matrices);
        _cellRandomValuesMap.Add(cell, randoms);
        RefitDecorationToNewCell(cell);
    }


    bool IsInNewCell()
    {
        Vector3 position = _cameraTransform.position;

        Vector2Int cell = new Vector2Int(Mathf.FloorToInt(position.x / cellSize),
            Mathf.FloorToInt(position.z / cellSize));

        if (cell != _currentCell)
        {
            _oldCell = _currentCell;
            _currentCell = cell;
            return true;
        }

        return false;
    }

    HashSet<Vector2Int> GetCellsWithinRadius(Vector2Int cell)
    {
        HashSet<Vector2Int> cells = new HashSet<Vector2Int>();

        for (int x = -cellRadius; x <= cellRadius; x++)
        {
            for (int z = -cellRadius; z <= cellRadius; z++)
            {
                Vector2Int cellCoordinates = new Vector2Int(x, z);

                if (cellCoordinates.magnitude > cellRadius)
                {
                    continue;
                }

                cells.Add(cellCoordinates + cell);
            }
        }

        return cells;
    }

    void CellCircleCalculation()
    {
        HashSet<Vector2Int> oldCellCircle = GetCellsWithinRadius(_oldCell);
        HashSet<Vector2Int> newCellCircle = GetCellsWithinRadius(_currentCell);
        List<Vector2Int> cellsToRemove = oldCellCircle.Except(newCellCircle).ToList();
        List<Vector2Int> cellsToAdd = newCellCircle.Except(oldCellCircle).ToList();

        for (int i = 0; i < cellsToAdd.Count; i++)
        {
            Vector2Int newCell = cellsToAdd[i];
            Vector2Int oldCell = cellsToRemove[i];

            _cellMatrices.Remove(_cellMap[oldCell]); // Remove the old cell matrices
            _cellKeys.Remove(oldCell);

            _cellMap[newCell] = _cellMap[oldCell];
            _cellRandomValuesMap[newCell] = _cellRandomValuesMap[oldCell];

            _cellMatrices.Add(_cellMap[newCell]); // Add the new cell matrices
            _cellKeys.Add(newCell);

            _cellMap.Remove(oldCell);
            _cellRandomValuesMap.Remove(oldCell);

            RefitDecorationToNewCell(newCell);
        }
    }

    void RefitDecorationToNewCell(Vector2Int cell)
    {
        int loopMax = Mathf.FloorToInt(Mathf.Sqrt(instancesPerCell));
        float adjustedMultiplier = 1f / Mathf.Pow(0.5f, power); // Balances the power
        Matrix4x4[] matrices = _cellMap[cell];
        var randoms = _cellRandomValuesMap[cell];

        for (int x = 0; x < loopMax; x++)
        {
            for (int z = 0; z < loopMax; z++)
            {
                int index = x * loopMax + z;
                var random = randoms[index];

                float scaledX = (x / (float) loopMax) * cellSize + cell.x * cellSize - cellSize / 2;
                float scaledZ = (z / (float) loopMax) * cellSize + cell.y * cellSize - cellSize / 2;
                var position = new Vector3(scaledX, 0, scaledZ);
                position += _offsets[index];

                float perlinValue =
                    Mathf.Pow(Mathf.PerlinNoise(position.x * noiseScale, position.z * noiseScale), power) *
                    multiplier * adjustedMultiplier;

                if (perlinValue > random)
                {
                    position.y = _terrain.SampleHeight(position) + _terrain.transform.position.y;
                    matrices[index].SetTRS(position, matrices[index].rotation, matrices[index].lossyScale);
                }
                else
                {
                    matrices[index].SetTRS(new Vector3(-1000, -1000, -1000), matrices[index].rotation,
                        matrices[index].lossyScale);
                }
            }
        }
    }

    void RenderMeshes()
    {
#if UNITY_EDITOR
        Marker.Begin();
#endif

        for (int i = 0; i < _cellMatrices.Count; i++)
        {
#if UNITY_EDITOR
            Marker2.Begin();
#endif
            Bounds bounds = ComputeCellBoundingVolume(_cellKeys[i]);

            _renderParams.worldBounds = bounds;
            var matrices = _cellMatrices[i];
#if UNITY_EDITOR
            Marker4.Begin();
#endif
            Graphics.RenderMeshInstanced(_renderParams, _instanceMesh, 0, matrices);
#if UNITY_EDITOR
            Marker4.End();
            Marker2.End();
#endif
        }
#if UNITY_EDITOR
        Marker.End();
#endif
    }

    private Bounds ComputeCellBoundingVolume(Vector2Int cell)
    {
#if UNITY_EDITOR
        Marker3.Begin();
#endif
        Vector3 center = new Vector3(cell.x * cellSize + cellSize * 0.5f, 0, cell.y * cellSize + cellSize * 0.5f);
        center.y = _terrain.SampleHeight(center) + _terrain.transform.position.y;
        var bounds = new Bounds(center, new Vector3(cellSize, cellSize, cellSize) + Vector3.one * cellBoundsPadding);
#if UNITY_EDITOR
        Marker3.End();
#endif
        return bounds;
    }

    // bool IsPositionInsideTerrainBounds(Vector3 position)
    // {
    //     return true;
    //     Vector3 terrainPosition = _terrain.transform.position;
    //     Vector3 terrainSize = _terrain.terrainData.size;
    //     Bounds terrainBounds = new Bounds(terrainPosition + terrainSize * 0.5f, terrainSize);
    //     return terrainBounds.Contains(position);
    // }
}