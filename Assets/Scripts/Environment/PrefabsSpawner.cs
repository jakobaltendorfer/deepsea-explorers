using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class PrefabsSpawner : MonoBehaviour
{
    [SerializeField] private int instancesPerCell = 100;
    [SerializeField] private float cellSize = 10f;
    [SerializeField] private List<GameObject> prefabs;
    [SerializeField] private int cellRadius = 2;
    [SerializeField] private float noiseScale = 0.1f;
    [SerializeField] private float scaleMax = 1f;
    [SerializeField] private float scaleMin = 0f;
    [SerializeField] private float power = 1f;
    [SerializeField] private float multiplier = 1f;
    [SerializeField] private float randomOffset = 1f;

    private Vector2Int _currentCell;
    private Vector2Int _oldCell;
    private Transform _cameraTransform;


    private Dictionary<Vector2Int, List<GameObject>> _cellMap = new Dictionary<Vector2Int, List<GameObject>>();
    private Dictionary<Vector2Int, List<float>> _cellMapRandomValues = new Dictionary<Vector2Int, List<float>>();

    private Terrain _terrain;


    void Start()
    {
        _cameraTransform = Camera.main.transform;
        instancesPerCell = (int) Mathf.Pow(Mathf.Ceil(Mathf.Sqrt(instancesPerCell)), 2);
        _terrain = GetComponent<Terrain>();
        IsInNewCell();
        InitializeCellMap();
    }

    void Update()
    {
        if (!IsInNewCell())
        {
            return;
        }

        CellCircleCalculation();
    }


    void InitializeCellMap()
    {
        HashSet<Vector2Int> cellCircle = GetCellsWithinRadius(_currentCell);

        foreach (Vector2Int cell in cellCircle)
        {
            PopulateCell(cell);
        }
    }

    void PopulateCell(Vector2Int cell)
    {
        List<GameObject> objects = new List<GameObject>();
        List<float> randoms = new List<float>();

        for (int i = 0; i < instancesPerCell; i++)
        {
            var prefab = prefabs[Random.Range(0, prefabs.Count)];
            GameObject newDecoration = Instantiate(prefab, Vector3.zero, Quaternion.identity, transform);
            // rescale
            float scale = Random.Range(scaleMin, scaleMax);
            newDecoration.transform.localScale = new Vector3(scale, scale, scale);

            // rotate
            Quaternion rotation = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
            newDecoration.transform.rotation = rotation;

            objects.Add(newDecoration);
            randoms.Add(Random.value);
        }

        _cellMap.Add(cell, objects);
        _cellMapRandomValues.Add(cell, randoms);
        RefitDecorationToNewCell(cell);
    }


    bool IsInNewCell()
    {
        Vector3 position = _cameraTransform.position;
        Vector2Int cell = new Vector2Int(Mathf.FloorToInt(position.x / cellSize),
            Mathf.FloorToInt(position.z / cellSize));

        if (cell != _currentCell)
        {
            _oldCell = _currentCell;
            _currentCell = cell;
            return true;
        }

        return false;
    }

    HashSet<Vector2Int> GetCellsWithinRadius(Vector2Int cell)
    {
        HashSet<Vector2Int> cells = new HashSet<Vector2Int>();

        for (int x = -cellRadius; x <= cellRadius; x++)
        {
            for (int z = -cellRadius; z <= cellRadius; z++)
            {
                Vector2Int cellCoordinates = new Vector2Int(x, z);

                if (cellCoordinates.magnitude > cellRadius)
                {
                    continue;
                }

                cells.Add(cellCoordinates + cell);
            }
        }

        return cells;
    }

    void CellCircleCalculation()
    {
        HashSet<Vector2Int> oldCellCircle = GetCellsWithinRadius(_oldCell);
        HashSet<Vector2Int> newCellCircle = GetCellsWithinRadius(_currentCell);
        List<Vector2Int> cellsToRemove = oldCellCircle.Except(newCellCircle).ToList();
        List<Vector2Int> cellsToAdd = newCellCircle.Except(oldCellCircle).ToList();

        for (int i = 0; i < cellsToAdd.Count; i++)
        {
            Vector2Int newCell = cellsToAdd[i];
            Vector2Int oldCell = cellsToRemove[i];
            _cellMap.Add(newCell, _cellMap[oldCell]);
            _cellMap.Remove(oldCell);
            _cellMapRandomValues.Add(newCell, _cellMapRandomValues[oldCell]);
            _cellMapRandomValues.Remove(oldCell);
            RefitDecorationToNewCell(newCell);
        }
    }

    void RefitDecorationToNewCell(Vector2Int cell)
    {
        int loopMax = Mathf.FloorToInt(Mathf.Sqrt(instancesPerCell));
        float adjustedMultiplier = 1f / Mathf.Pow(0.5f, power); // Balances the power

        for (int x = 0; x < loopMax; x++)
        {
            for (int z = 0; z < loopMax; z++)
            {
                int index = x * loopMax + z;

                float scaledX = (x / (float) loopMax) * cellSize + cell.x * cellSize - cellSize / 2;
                float scaledZ = (z / (float) loopMax) * cellSize + cell.y * cellSize - cellSize / 2;

                scaledX += Random.Range(0, randomOffset);
                scaledZ += Random.Range(0, randomOffset);


                float perlinValue = Mathf.Pow(Mathf.PerlinNoise(scaledX * noiseScale, scaledZ * noiseScale), power) *
                                    multiplier * adjustedMultiplier;
                GameObject prefab = _cellMap[cell][index];
                var position = new Vector3(scaledX, 0, scaledZ);
                if (perlinValue > _cellMapRandomValues[cell][index])
                {
                    position.y = _terrain.SampleHeight(position) + _terrain.transform.position.y;
                    float normalizedX = (position.x - _terrain.transform.position.x) / _terrain.terrainData.size.x;
                    float normalizedZ = (position.z - _terrain.transform.position.z) / _terrain.terrainData.size.z;
                    Vector3 normal = _terrain.terrainData.GetInterpolatedNormal(normalizedX, normalizedZ);
                    prefab.transform.up = normal;
                    prefab.transform.position = position;
                    prefab.SetActive(true);
                }
                else
                {
                    prefab.SetActive(false);
                }
            }
        }
    }

    bool IsPositionInsideTerrainBounds(Vector3 position)
    {
        Vector3 terrainPosition = _terrain.transform.position;
        Vector3 terrainSize = _terrain.terrainData.size;
        Bounds terrainBounds = new Bounds(terrainPosition + terrainSize * 0.5f, terrainSize);
        return terrainBounds.Contains(position);
    }
}