using System;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class NameReader : MonoBehaviour
{
    public static NameReader Instance { get; private set; }

    // List to hold the names read from the file.
    private readonly List<string> _namesList = new List<string>();

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        ReadNamesFromFile("Dialogue/names"); // 'names' is the name of the txt file without the extension
    }

    void ReadNamesFromFile(string fileName)
    {
        // Load the TextAsset from the Resources folder
        TextAsset txtAsset = Resources.Load<TextAsset>(fileName);

        if (txtAsset == null)
        {
            Debug.LogError("File not found!");
            return;
        }

        // Split the file into lines and store each line as an item in the list
        _namesList.AddRange(txtAsset.text.Split('\n'));
    }

    public string GetRandomName()
    {
        return _namesList[Random.Range(0, _namesList.Count)].Replace("\r", string.Empty);
    }
}