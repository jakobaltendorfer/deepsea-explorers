using System;
using System.Collections.Generic;
using System.Linq;
using Networking;
using TMPro;
using Unity.Services.Authentication;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI
{
    public class MenuScript : MonoBehaviour
    {
        [SerializeField] private TMP_InputField lobbyCode;
        [SerializeField] private List<Transform> playerUis;
        [SerializeField] private Transform menu;
        [SerializeField] private Transform start;
        [SerializeField] private Transform lobby;
        [SerializeField] private Transform startGameButton;
        [SerializeField] private Sprite captainSprite;
        [SerializeField] private Sprite crew1Sprite;
        [SerializeField] private Sprite crew2Sprite;

        private List<Player> _players = new List<Player>();


        private void Start()
        {
            LobbyManager.Instance.OnPlayersUpdate += OnPlayerUpdate;

            menu.gameObject.SetActive(true);
            start.gameObject.SetActive(false);
            lobby.gameObject.SetActive(false);

            startGameButton.gameObject.SetActive(false);

            SetRoleDropdowns();
        }

        private void SetRoleDropdowns()
        {
            var dropdowns = GetComponentsInChildren<TMP_Dropdown>();
            if (dropdowns.Length == 0) return;

            var rolesList = ((RoleController.Role[]) Enum.GetValues(typeof(RoleController.Role))).ToList();
            List<TMP_Dropdown.OptionData> options = rolesList.ConvertAll(role => new TMP_Dropdown.OptionData
            {
                text = RoleController.roleToText[role]
            });

            foreach (var dropdown in dropdowns)
            {
                dropdown.options = options;
                dropdown.value = 1;
            }

            dropdowns[0].value = 0;
        }

        private bool CheckPlayers()
        {
            int numCaptains = 0;
            foreach (var player in _players)
            {
                var data = player.Data;
                var playerName = data[LobbyManager.PlayerNameKey].Value;
                if (playerName.Length == 0) return false;

                var role = (RoleController.Role) int.Parse(player.Data[LobbyManager.PlayerRoleKey].Value);
                if (role == RoleController.Role.Captain)
                    numCaptains++;
            }


            return numCaptains == 1;
        }

        // ReSharper disable Unity.PerformanceAnalysis
        private void OnPlayerUpdate(object sender, LobbyManager.OnPlayerUpdateEventArgs e)
        {
            // enable start game button only on host
            if (startGameButton == null || startGameButton.gameObject == null) return;
            _players = e.players;

            startGameButton.gameObject.SetActive(LobbyManager.Instance.IsLobbyHost() && CheckPlayers());

            bool foundCrewMember = false;
            foreach (var player in _players)
            {
                int index = _players.IndexOf(player);
                var playerUI = playerUis[index];
                playerUI.gameObject.SetActive(true);
                var data = player.Data;

                RoleController.Role role = (RoleController.Role) int.Parse(data[LobbyManager.PlayerRoleKey].Value);

                var playerImage = playerUI.GetComponentInChildren<MenuImageTag>().GetComponent<Image>();
                var sprite = captainSprite;
                if (role == RoleController.Role.Crewmember)
                {
                    if (!foundCrewMember)
                    {
                        sprite = crew1Sprite;
                        foundCrewMember = true;
                    }
                    else
                    {
                        sprite = crew2Sprite;
                    }
                }

                playerImage.sprite = sprite;

                var dropdown = playerUI.GetComponentInChildren<TMP_Dropdown>();
                var nameText = playerUI.GetComponentInChildren<TMP_Text>();
                var input = playerUI.GetComponentInChildren<TMP_InputField>();
                var editButton = playerUI.GetChild(0).Find("Edit");

                bool isCurrentPlayer = player.Id == AuthenticationService.Instance.PlayerId;

                dropdown.interactable = isCurrentPlayer;
                // input.gameObject.SetActive(isCurrentPlayer);
                // nameText.gameObject.SetActive(!isCurrentPlayer);
                if (editButton)
                    editButton.gameObject.SetActive(isCurrentPlayer);

                if (data.ContainsKey(LobbyManager.PlayerRoleKey))
                    dropdown.value = (int) role;
                if (data.ContainsKey(LobbyManager.PlayerNameKey))
                {
                    if (nameText)
                        nameText.text = data[LobbyManager.PlayerNameKey].Value;
                }
            }

            // set empty slots to disabled
            for (int i = _players.Count; i < playerUis.Count; i++)
            {
                var playerUI = playerUis[i];
                playerUI.gameObject.SetActive(false);
            }

            lobby.GetComponentInChildren<TMP_Text>().text = "Code: " + LobbyManager.Instance.GetLobbyCode();
        }

        public void StartGame()
        {
            lobby.gameObject.SetActive(false);
            LobbyManager.Instance.StartGame();
        }

        public void OnStartPressed()
        {
            menu.gameObject.SetActive(false);
            start.gameObject.SetActive(true);
        }

        public void BackButtonPressed(int state)
        {
            switch (state)
            {
                // start back to menu
                case 0:
                    start.gameObject.SetActive(false);
                    menu.gameObject.SetActive(true);
                    break;
                // host lobby back to start
                case 1:
                    lobby.gameObject.SetActive(false);
                    start.gameObject.SetActive(true);
                    LobbyManager.Instance.LeaveLobby();
                    break;
                default:
                    return;
            }
        }

        public void QuitApp()
        {
            Application.Quit();
        }

        public async void OnHostLobby()
        {
            await LobbyManager.Instance.CreateLobby();
            start.gameObject.SetActive(false);
            lobby.gameObject.SetActive(true);
        }

        public async void OnJoin()
        {
            if (lobbyCode.text.Length == 0) return;

            bool res = await LobbyManager.Instance.JoinLobby(lobbyCode.text);
            if (!res) return;

            start.gameObject.SetActive(false);
            lobby.gameObject.SetActive(true);
        }

        public async void OnQuickJoin()
        {
            bool res = await LobbyManager.Instance.QuickJoinLobby();
            if (!res) return;

            start.gameObject.SetActive(false);
            lobby.gameObject.SetActive(true);
        }

        public void OnPlayerRoleChanged()
        {
            var player = GetCurrentPlayer();
            int index = _players.IndexOf(player);

            var dropdown = playerUis[index].GetComponentInChildren<TMP_Dropdown>();
            int value = dropdown.value;
            Debug.Log("OnPlayerRoleChanged " + index + " Value: " + value);

            LobbyManager.Instance.UpdatePlayerRole((RoleController.Role) value);

            startGameButton.gameObject.SetActive(LobbyManager.Instance.IsLobbyHost() && CheckPlayers());
        }

        public void OnEditPressed()
        {
            var player = GetCurrentPlayer();

            int index = _players.IndexOf(player);
            var playerUI = playerUis[index];
            playerUI.gameObject.SetActive(true);

            var nameText = playerUI.GetChild(0).GetChild(0);
            var input = playerUI.GetChild(0).GetChild(1);
            var editButton = playerUI.GetChild(0).Find("Edit");

            editButton.GetChild(0).gameObject.SetActive(false);
            editButton.GetChild(1).gameObject.SetActive(true);

            input.gameObject.SetActive(true);
            input.GetComponent<TMP_InputField>().text = nameText.GetComponent<TMP_Text>().text;
            nameText.gameObject.SetActive(false);
        }

        private Player GetCurrentPlayer()
        {
            foreach (var player in _players)
            {
                if (player.Id == AuthenticationService.Instance.PlayerId) return player;
            }

            return null;
        }

        public void OnNameSubmit()
        {
            var player = GetCurrentPlayer();

            int index = _players.IndexOf(player);
            var playerUI = playerUis[index];
            playerUI.gameObject.SetActive(true);

            var nameText = playerUI.GetChild(0).GetChild(0);
            var input = playerUI.GetChild(0).GetChild(1);
            var editButton = playerUI.GetChild(0).Find("Edit");

            editButton.GetChild(0).gameObject.SetActive(true);
            editButton.GetChild(1).gameObject.SetActive(false);

            nameText.gameObject.SetActive(true);
            var newName = input.GetComponent<TMP_InputField>().text;
            nameText.GetComponent<TMP_Text>().text = newName;
            input.gameObject.SetActive(false);

            LobbyManager.Instance.UpdatePlayerName(newName);


            startGameButton.gameObject.SetActive(LobbyManager.Instance.IsLobbyHost() && CheckPlayers());
        }
    }
}