using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class AmmoBox : MonoBehaviour
{
    public enum AmmoType
    {
        Default,
        Special
    }

    public AmmoType bulletType = AmmoType.Default;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!other.GetComponent<NetworkObject>().IsOwner) return;

            if (other.gameObject.TryGetComponent<PlayerController>(out PlayerController playerController))
            {
                if (bulletType == AmmoType.Default)
                    playerController.pickedAmmo = true;
                else
                    playerController.pickedSpecialAmmo = true;

                PlayerCanvas.Instance.EnablePickUpAmmo(bulletType, true);
            }
        }
    }
}