using System;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private Transform pauseMenuTransform;

    // Start is called before the first frame update
    void Start()
    {
        pauseMenuTransform.gameObject.SetActive(false);
        GameInput.Instance.OnPauseAction += OnPause;
    }

    private void OnPause(object sender, EventArgs e)
    {
        bool active = pauseMenuTransform.gameObject.activeSelf;
        if (active)
        {
            Cursor.visible = false;
        }
        else
        {
            Cursor.visible = true;
        }

        pauseMenuTransform.gameObject.SetActive(!active);
    }

    public void OnQuitPressed()
    {
        Debug.Log("OnQuitPressed");
        Application.Quit();
    }
}