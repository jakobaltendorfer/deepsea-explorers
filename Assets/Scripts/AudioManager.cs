using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class AudioManager : NetworkBehaviour
{
    public static AudioManager Instance { get; private set; }

    [SerializeField] private List<AudioSource> backgroundMusic;
    [SerializeField] private AudioSource dangerousMusic;
    [SerializeField] private AudioSource music;
    [SerializeField] private float switchTime = 4f;

    private float _dangerousVolume,
        _musicVolume,
        _targetVolumeDangerous,
        _targetVolumeMusic,
        _startVolumeDangerous,
        _startVolumeMusic;

    private bool _stopping = true;

    private float _timer = 0f;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    private void Start()
    {
        _dangerousVolume = dangerousMusic.volume;
        _musicVolume = music.volume;
        dangerousMusic.volume = 0f;
        music.volume = 0f;
        _targetVolumeMusic = _musicVolume;

        _startVolumeDangerous = dangerousMusic.volume;
        _startVolumeMusic = music.volume;
    }

    public void Play()
    {
        foreach (var sound in backgroundMusic)
            sound.Play();
    }

    public void PlayDangerous()
    {
        PlayDangerousClientRpc();
    }

    [ClientRpc]
    void PlayDangerousClientRpc()
    {
        if (!_stopping) return;

        dangerousMusic.volume = 0f;
        music.volume = _musicVolume;

        _timer = 0f;
        _startVolumeDangerous = dangerousMusic.volume;
        _startVolumeMusic = music.volume;


        _targetVolumeDangerous = _dangerousVolume;
        _targetVolumeMusic = 0f;

        _stopping = false;
    }


    public void StopDangerous()
    {
        StopDangerousClientRpc();
    }

    [ClientRpc]
    void StopDangerousClientRpc()
    {
        if (_stopping) return;

        _timer = 0f;
        _startVolumeDangerous = dangerousMusic.volume;
        _startVolumeMusic = music.volume;

        _targetVolumeDangerous = 0f;
        _targetVolumeMusic = _musicVolume;
        _stopping = true;
    }

    private void Update()
    {
        _timer += Time.unscaledDeltaTime;

        dangerousMusic.volume = Mathf.Lerp(_startVolumeDangerous, _targetVolumeDangerous, _timer / switchTime);
        music.volume = Mathf.Lerp(_startVolumeMusic, _targetVolumeMusic, _timer / switchTime);
    }
}