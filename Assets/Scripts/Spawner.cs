using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Netcode;
using UnityEngine;

public class Spawner : NetworkBehaviour
{
    [SerializeField] private ShutterDoor door;
    [SerializeField] private GameObject enemySubmarine;
    [SerializeField] private Transform spawnPosition;
    [SerializeField] private float coolDown = 5f;

    private float timeSinceDispatch;
    private bool isDelaying;

    // Update is called once per frame
    void Update()
    {
        if (!IsHost) return;

        if (isDelaying)
        {
            // Increment the delay timer
            timeSinceDispatch += Time.deltaTime;

            // Check if the delay duration has been reached
            if (timeSinceDispatch >= coolDown)
            {
                // Execute the delayed command
                door.requestStateChange = true;

                // Reset the delay variables
                isDelaying = false;
                timeSinceDispatch = 0.0f;
            }

            return;
        }
    }

    public void SpawnSubmarine()
    {
        door.requestStateChange = true;
        GameObject go = Instantiate(enemySubmarine, spawnPosition.position, spawnPosition.rotation);
        go.GetComponent<NetworkObject>().Spawn();
        EnemySpawner.Instance.AddChildToList(go);
        timeSinceDispatch = 0.0f;
        isDelaying = true;

        AudioManager.Instance.PlayDangerous();
    }
}