using Unity.Collections;
using Unity.Entities;
using Unity.Scenes;
using UnityEngine;

public partial class SceneLoaderSystem : SystemBase
{
    private EntityQuery _newRequests;

    protected override void OnCreate()
    {
        _newRequests = GetEntityQuery(typeof(SceneLoader));
    }

    protected override void OnUpdate()
    {
        var requests = _newRequests.ToComponentDataArray<SceneLoader>(Allocator.Temp);
        // Can't use a foreach with a query as SceneSystem.LoadSceneAsync does structural changes
        for (int i = 0; i < requests.Length; i += 1)
        {
            SceneSystem.LoadSceneAsync(World.Unmanaged, requests[i].SceneReference);
        }

        requests.Dispose();
        EntityManager.DestroyEntity(_newRequests);
    }
}
