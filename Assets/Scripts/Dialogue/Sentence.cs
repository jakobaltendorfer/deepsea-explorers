﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// == Object
[System.Serializable]
public class Sentence
{
    public int player;
    public string sentence;
    public char side;
    
    public Sentence(int _player, string _sentence, char _side)
    {
        player = _player;
        sentence = _sentence;
        side = _side;
    }
}