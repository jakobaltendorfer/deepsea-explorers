using System;
using System.Collections;
using UnityEngine;

public class MessageAnimation : MonoBehaviour
{
    [SerializeField] float animationDuration = 1.0f;
    private Vector3 _initialScale, _targetScale;
    private float _startTime;
    private bool _animationDone;
    private RectTransform _rectTransform;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    public void StartAnimateSquish(float from, float to, bool active)
    {
        if (gameObject.activeSelf)
            StartCoroutine(AnimateSquish(new Vector3(from, 1.0f, 1.0f), new Vector3(to, 1.0f, 1.0f), active));
    }

    public void StartAnimateAppear(float from, float to, bool active)
    {
        if (gameObject.activeSelf)
            StartCoroutine(AnimateAppear(from, to, active));
    }

    private IEnumerator AnimateAppear(float from, float to, bool active) //-350, 100
    {
        float journey = 0f;

        while (journey <= animationDuration)
        {
            journey += Time.unscaledDeltaTime;
            float percent = Mathf.Clamp01(journey / animationDuration);

            float newPosition = Mathf.Lerp(from, to, percent);
            _rectTransform.anchoredPosition = new Vector2(_rectTransform.anchoredPosition.x, newPosition);

            yield return null;
        }

        yield return new WaitForSeconds(5);
        gameObject.SetActive(active);
    }

    private IEnumerator AnimateSquish(Vector3 origin, Vector3 target, bool active)
    {
        float journey = 0f;
        while (journey <= animationDuration)
        {
            journey += Time.unscaledDeltaTime;
            float percent = Mathf.Clamp01(journey / animationDuration);

            Vector3 newScale = Vector3.Lerp(origin, target, percent);
            transform.localScale = newScale;

            yield return null;
        }

        yield return new WaitForSeconds(5);

        gameObject.SetActive(active);
    }
}