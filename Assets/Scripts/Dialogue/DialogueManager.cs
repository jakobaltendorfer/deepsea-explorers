using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cinemachine;
using Networking;
using Submarine;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class DialogueManager : NetworkBehaviour
{
    public static DialogueManager Instance { get; private set; }

    [SerializeField] private GameObject leftDialogue, rightDialogue, messageDialogue, missionDialogue, debugWindow;
    [SerializeField] private bool showDebug = true;
    [SerializeField] private TMP_Dropdown ddAddMissions, ddRemoveMissions, ddDialogues;
    [SerializeField] private float typeSpeed = 100;
    
    public GameObject task;

    public event EventHandler<OnMissionStartEventArgs> OnMissionStart;
    public event EventHandler<OnMissionStartEventArgs> OnMissionFinished;
    public event EventHandler<OnDialogFinishedEventArgs> OnDialogFinished;

    public class OnMissionStartEventArgs : EventArgs
    {
        public string missionId;
    }

    public class OnDialogFinishedEventArgs : EventArgs
    {
        public string dialogueID;
    }

    private List<string> _names = new List<string>(),
        _dialogueNames = new List<string>(),
        _missionNames = new List<string>();

    private List<Sprite> _sprites = new List<Sprite>();
    private Queue<Sentence> _sentences = new Queue<Sentence>();
    private Dictionary<string, List<Sentence>> _entireDialogue = new Dictionary<string, List<Sentence>>();
    private Dictionary<string, string> _missionDict = new Dictionary<string, string>();

    private TMP_Text _leftName, _rightName, _leftDialogueText, _rightDialogueText, _messageText, _currentTextBox;
    private Image _leftImage, _rightImage;
    private Sprite _spritePlayer1, _spritePlayer2, _spritePlayer3, _spriteEvilCompany, _spriteReporter;
    private string _nameCaptain, _nameMember1, _nameMember2, _nameEvilCompany, _nameReporter, _lastDialogue;
    private bool _dialogueCurrentlyActive, _leftAnimated, _rightAnimated, _messageAnimated;
    private int _activeMissions = 0;
    private Coroutine _typeCoroutine;

    private readonly HashSet<string> _alreadyAddedMissions = new HashSet<string>();
    private readonly HashSet<string> _alreadyAddedDialogs = new HashSet<string>();

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    private void Start()
    {
        GameInput.Instance.OnNextDialogueAction += OnNextDialoguePerformed;
        SetGameObjectsActive(false, messageDialogue, rightDialogue, leftDialogue, missionDialogue);
        SetCharacters();
        CreateLists();
        GetDialogueComponents();
        _entireDialogue = DialogueParser.CreateDialogue(_nameCaptain, _nameMember1, _nameMember2);
        _missionDict = DialogueParser.CreateMissions();
        DebugOptions();

        // if (!skipDialogue)
        //     StartDialogue("Start");
    }

    private void DebugOptions()
    {
        _dialogueNames = _entireDialogue.Keys.ToList();
        _missionNames = _missionDict.Keys.ToList();
        ddDialogues.AddOptions(_dialogueNames);
        ddAddMissions.AddOptions(_missionNames);
        ddRemoveMissions.AddOptions(_missionNames);
    }

    private void SetGameObjectsActive(bool state, params GameObject[] objects)
    {
        foreach (var gameObject in objects)
        {
            gameObject.SetActive(state);
        }
    }

    private void Update()
    {
        debugWindow.SetActive(showDebug);
    }

    private void GetDialogueComponents()
    {
        _leftName = leftDialogue.transform.Find("NameOuter").Find("NameInner").Find("NameText")
            .GetComponent<TMP_Text>();
        _rightName = rightDialogue.transform.Find("NameOuter").Find("NameInner").Find("NameText")
            .GetComponent<TMP_Text>();
        _leftDialogueText = leftDialogue.transform.Find("DialogueOuter").Find("DialogueInner").Find("DialogueText")
            .GetComponent<TMP_Text>();
        _rightDialogueText = rightDialogue.transform.Find("DialogueOuter").Find("DialogueInner").Find("DialogueText")
            .GetComponent<TMP_Text>();
        _leftImage = leftDialogue.transform.Find("Character").GetComponent<Image>();
        _rightImage = rightDialogue.transform.Find("Character").GetComponent<Image>();
        _messageText = messageDialogue.transform.Find("MessageOuter").Find("MessageInner").Find("MessageText")
            .GetComponent<TMP_Text>();
    }

    private void CreateLists()
    {
        _names.AddRange(new List<string>
            {_nameCaptain, _nameMember1, _nameMember2, _nameEvilCompany, _nameReporter});

        _sprites.AddRange(new List<Sprite>
            {_spritePlayer1, _spritePlayer2, _spritePlayer3, _spriteEvilCompany, _spriteReporter});
    }

    private void SetCharacters()
    {
        _nameCaptain = "Captain";
        _nameMember1 = "Crewmember 1";
        _nameMember2 = "Crewmember 2";

        if (LobbyManager.Instance != null)
        {
            var players = LobbyManager.Instance.GetLobbyPlayers();
            foreach (var player in players)
            {
                var role = (RoleController.Role) int.Parse(player.Data[LobbyManager.PlayerRoleKey].Value);
                var playerName = player.Data[LobbyManager.PlayerNameKey].Value;
                if (role == RoleController.Role.Captain)
                    _nameCaptain = playerName;
                else if (string.IsNullOrEmpty(_nameMember1))
                    _nameMember1 = playerName;
                else
                    _nameMember2 = playerName;
            }
        }
        else
        {
            _nameCaptain = "Min";
            _nameMember1 = "Ropi";
            _nameMember2 = "Emma";
        }

        //todo does playerpref work with netcode
        _spritePlayer1 = Resources.Load<Sprite>("Dialogue/min"); //(PlayerPrefs.GetString("player1Filename"));
        _spritePlayer2 = Resources.Load<Sprite>("Dialogue/emma"); //(PlayerPrefs.GetString("player2Filename"));
        _spritePlayer3 = Resources.Load<Sprite>("Dialogue/ropi"); //(PlayerPrefs.GetString("player3Filename"));

        _spriteReporter = Resources.Load<Sprite>("Dialogue/reporter 1"); //todo sprites -> resources
        _nameReporter = "Reporter";

        _spriteEvilCompany = Resources.Load<Sprite>("Dialogue/evil");
        _nameEvilCompany = "PetrolCorp";
    }

    private void OnNextDialoguePerformed(object sender, EventArgs e)
    {
        if (_dialogueCurrentlyActive && PlayerController.LocalPlayer.IsCaptain()) //todo check if captain
            DisplayNextSentenceServerRpc();
    }


    private IEnumerator AnimateAndDestroy(GameObject missionTextObject)
    {
        TextMeshProUGUI tmpText = missionTextObject.GetComponentInChildren<TextMeshProUGUI>();

        // Apply strikethrough formatting to the text
        tmpText.text = $"<s>{tmpText.text}</s>";

        yield return new WaitForSeconds(1.5f); // Wait for the strikethrough duration
        Destroy(missionTextObject);
        _activeMissions--;

        if (_activeMissions == 0)
            missionDialogue.SetActive(false);
    }

    [ServerRpc(RequireOwnership = false)]
    public void RemoveMissionServerRpc(string missionID)
    {
        if (missionID == "Debug") missionID = ProcessDropDown(ddRemoveMissions);
        RemoveMissionClientRpc(missionID);
    }

    private string ProcessDropDown(TMP_Dropdown dropDown)
    {
        var chosenOption = dropDown.options[dropDown.value].text;
        dropDown.SetValueWithoutNotify(0);
        return chosenOption;
    }


    [ClientRpc]
    private void RemoveMissionClientRpc(string missionID)
    {
        var mission = missionDialogue.transform.Find(missionID);
        if (!mission) return;
        OnMissionFinished?.Invoke(this, new OnMissionStartEventArgs {missionId = missionID});
        StartCoroutine(AnimateAndDestroy(mission.gameObject));
    }

    public void StartDialogue(string dialogueId)
    {
        if (dialogueId == "Debug") dialogueId = ProcessDropDown(ddDialogues);
        StartDialogueServerRpc(dialogueId);
    }

    [ServerRpc(RequireOwnership = false)]
    private void StartDialogueServerRpc(string dialogueId)
    {
        if (_alreadyAddedDialogs.Contains(dialogueId)) return;
        _alreadyAddedDialogs.Add(dialogueId); // Cant add dialog twice

        // enemies/spawner/turret to idle
        EnemySpawner.Instance.SetEnemiesToIdle(true);


        StartDialogueClientRpc(dialogueId);
    }

    [ClientRpc]
    private void StartDialogueClientRpc(string dialogueId)
    {
        _lastDialogue = dialogueId;

        List<Sentence> dialogue = _entireDialogue[dialogueId];
        if (dialogue == null) return;

        _dialogueCurrentlyActive = true;
        GameInput.Instance.Disable();
        CameraController.Instance.DisableGunCameraMovement();
        SubmarineVisual.Instance.StartEngine(false);

        Time.timeScale = 0f;

        _sentences.Clear();

        foreach (var sentence in dialogue)
        {
            _sentences.Enqueue(sentence);
        }

        DisplayNextSentenceClientRpc();
    }

    [ServerRpc(RequireOwnership = false)]
    public void DisplayNextSentenceServerRpc()
    {
        DisplayNextSentenceClientRpc();
    }

    [ClientRpc]
    private void DisplayNextSentenceClientRpc()
    {
        if (_sentences.Count == 0) //todo think about adding networked bool if something goes wrong -< end dialogue
        {
            if (IsHost)
                EndDialogue();
            return;
        }

        var sentence = _sentences.Dequeue();
        var player = sentence.player - 1;

        Debug.Log("Player: " + player);

        if (player == 8) //mission
        {
            AddMissionClientRpc(sentence.sentence);
            if (IsHost)
                DisplayNextSentenceClientRpc();
            return;
        }

        if (player == -1)
        {
            SetGameObjectsActive(false, leftDialogue, rightDialogue);
            SetGameObjectsActive(true, messageDialogue);
            if (!_messageAnimated)
            {
                _messageAnimated = true;
                messageDialogue.GetComponent<MessageAnimation>().StartAnimateSquish(0f, 1f, true);
            }

            _currentTextBox = _messageText;
        }
        else if (sentence.side == 'l')
        {
            SetGameObjectsActive(false, rightDialogue, messageDialogue);
            SetGameObjectsActive(true, leftDialogue);
            if (!_leftAnimated)
            {
                _leftAnimated = true;
                leftDialogue.GetComponent<MessageAnimation>().StartAnimateAppear(-350, 100, true);
            }

            _leftImage.sprite = _sprites[player];
            _leftName.text = _names[player];
            _currentTextBox = _leftDialogueText;
        }
        else
        {
            SetGameObjectsActive(false, messageDialogue, leftDialogue);
            SetGameObjectsActive(true, rightDialogue);
            if (!_rightAnimated)
            {
                rightDialogue.GetComponent<MessageAnimation>().StartAnimateAppear(-350, 100, true);
                _rightAnimated = true;
            }

            _rightImage.sprite = _sprites[player];
            _rightName.text = _names[player];
            _currentTextBox = _rightDialogueText;
        }

        if (_typeCoroutine != null)
        {
            StopCoroutine(_typeCoroutine);
        }

        _typeCoroutine = StartCoroutine(TypeSentence(sentence.sentence));
    }

    [ServerRpc(RequireOwnership = false)]
    public void AddMissionServerRpc(string taskID)
    {
        if (taskID != "Debug" && _alreadyAddedMissions.Contains(taskID)) return;
        _alreadyAddedMissions.Add(taskID); // Cant add mission twice

        if (taskID == "Debug") taskID = ProcessDropDown(ddAddMissions);
        AddMissionClientRpc(taskID);
    }

    [ClientRpc]
    private void AddMissionClientRpc(string taskID)
    {
        OnMissionStart?.Invoke(this, new OnMissionStartEventArgs {missionId = taskID});

        var newTask = Instantiate(task, missionDialogue.transform);
        newTask.GetComponentInChildren<TMP_Text>().SetText(_missionDict[taskID]);
        newTask.name = taskID;
        _activeMissions++;
        if (!missionDialogue.activeSelf)
            missionDialogue.SetActive(true);
        //erinnern: falls subtask -> scale auf 0.75 schaut gut aus
    }


    private IEnumerator TypeSentence(string sentence)
    {
        float t = 0;
        var charIndex = 0;

        while (charIndex < sentence.Length)
        {
            t += Time.unscaledDeltaTime * typeSpeed;
            charIndex = Mathf.FloorToInt(t);
            charIndex = Mathf.Clamp(charIndex, 0, sentence.Length);

            _currentTextBox.text = sentence.Substring(0, charIndex);
            yield return null;
        }
    }

    private void DeactivateAnimations()
    {
        if (_rightAnimated) rightDialogue.GetComponent<MessageAnimation>().StartAnimateAppear(100f, -350f, false);
        if (_leftAnimated) leftDialogue.GetComponent<MessageAnimation>().StartAnimateAppear(100f, -350f, false);
        if (_messageAnimated) messageDialogue.GetComponent<MessageAnimation>().StartAnimateSquish(1f, 0f, false);
        _leftAnimated = false;
        _rightAnimated = false;
        _messageAnimated = false;
    }

    public void EndDialogue()
    {
        EndDialogServerRpc();
    }

    [ServerRpc(RequireOwnership = false)]
    private void EndDialogServerRpc()
    {
        EndDialogClientRpc();
    }

    [ClientRpc]
    private void EndDialogClientRpc()
    {
        OnDialogFinished?.Invoke(this, new OnDialogFinishedEventArgs {dialogueID = _lastDialogue});
        DeactivateAnimations();
        GameInput.Instance.Enable();
        CameraController.Instance.EnableGunCameraMovement();
        SubmarineVisual.Instance.StartEngine(true);
        _dialogueCurrentlyActive = false;

        EnemySpawner.Instance.SetEnemiesToIdle(false);

        Time.timeScale = 1f;
    }
}