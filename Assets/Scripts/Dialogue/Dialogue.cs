using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// == Object
[System.Serializable]
public class Dialogue
{
    public List<Sentence> dialogue = new List<Sentence>();
}
