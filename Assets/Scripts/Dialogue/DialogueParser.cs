﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;

public class DialogueParser : MonoBehaviour
{
    public static Dictionary <string, string> CreateMissions()
    {
        Dictionary <string, string> taskDict = new Dictionary < string, string > ();
        TextAsset textAsset = Resources.Load<TextAsset>("Dialogue/missions");

        if (textAsset == null) return null;
        
        using StringReader reader = new StringReader(textAsset.text);
        string line;
        while ((line = reader.ReadLine()) != null)
        {
            string[] pair = line.Split('|');
            if (pair.Length == 2)
            {
                taskDict[pair[0]] = pair[1];
            }
        }

        return taskDict;
    }
    
    public static Dictionary<string, List<Sentence>> CreateDialogue(string namePlayer1, string namePlayer2, string namePlayer3)
    {
        Dictionary<string, List<Sentence>> entireDialogue = new Dictionary<string, List<Sentence>>();
        
        var mytxtData = (TextAsset)Resources.Load("Dialogue/dialogue");
        var content = mytxtData.text;
        content = content.Replace("\r\n", string.Empty); 
        content = content.Replace("PLAYER1", namePlayer1); //Captain
        content = content.Replace("PLAYER2", namePlayer2);
        content = content.Replace("PLAYER3", namePlayer3);


        var listDialogues = content.Split('$').ToList();
        listDialogues.RemoveAt(0); // comment 
        foreach (var dialogue in listDialogues)
        {
            var listSentences = dialogue.Split('@').ToList();
            var dialogueType = listSentences[0];
            entireDialogue.Add(dialogueType, new List<Sentence>());
            for (var i = 1; i < listSentences.Count; i++)
            {
                var sentenceSplit = listSentences[i].Split('|').ToList();
                var player = int.Parse(sentenceSplit[0].Substring(0, 1));
                var side = char.Parse(sentenceSplit[0].Substring(1, 1));
                var sentence = sentenceSplit[1];
                var newSentence = new Sentence(player, sentence, side);
                entireDialogue[dialogueType].Add(newSentence);
            }
        }

        return entireDialogue;
    }
}