using System.Collections;
using System.Collections.Generic;
using Submarine;
using Unity.Netcode;
using UnityEngine;

public class QuestController : NetworkBehaviour
{
    [SerializeField] private Transform riffQuest;
    [SerializeField] private Transform unarmedRigQuest;
    [SerializeField] private Transform armedRigQuest;
    [SerializeField] private Transform templeQuest;
    [SerializeField] private GameObject endScreen;


    void Start()
    {
        DialogueManager.Instance.OnMissionStart += OnMissionStart;
        DialogueManager.Instance.OnMissionFinished += OnMissionFinished;
        DialogueManager.Instance.OnDialogFinished += OnDialogFinished;
    }

    private void EndGame()
    {
        endScreen.SetActive(true);
        GameInput.Instance.Disable();
    }

    private void OnDialogFinished(object sender, DialogueManager.OnDialogFinishedEventArgs e)
    {
        if (e.dialogueID == "DataSecured")
        {
            Invoke(nameof(EndGame), 3f);
        }

        if (e.dialogueID == "EvidenceSentToReporter")
        {
            Invoke(nameof(TriggerEnemies), 10f);
        }

        if (e.dialogueID == "CompanyAttack")
        {
            EnemySpawner.Instance.Spawn(0f);
        }

        if (e.dialogueID == "TemplePhoto")
        {
            Invoke(nameof(EnemiesAfterTemple), 35f);
        }

        if (e.dialogueID == "AllEnemiesDefeated")
        {
            Invoke(nameof(TriggerUnknownStructure), 10f);
        }

        if (e.dialogueID == "CoralreefDestroyed")
        {
            var ammos = GameObject.FindGameObjectsWithTag("Ammo");
            foreach (var ammo in ammos)
            {
                var ammoBox = ammo.GetComponent<AmmoBox>();
                if (ammoBox != null && ammoBox.bulletType == AmmoBox.AmmoType.Special)
                {
                    ammo.SetActive(false);
                }
            }
        }

        if (e.dialogueID == "Start")
        {
            SubmarineVisual.Instance.SwitchLights(true);
        }
    }

    private void EnemiesAfterTemple()
    {
        EnemySpawner.Instance.Spawn(0f);
    }

    private void TriggerUnknownStructure()
    {
        DialogueManager.Instance.StartDialogue("UnknownStructure");
    }

    private void TriggerEnemies()
    {
        DialogueManager.Instance.StartDialogue("CompanyAttack");
    }


    private void OnMissionStart(object sender, DialogueManager.OnMissionStartEventArgs e)
    {
        Debug.Log("Started " + e.missionId);
        if (e.missionId == "coralreef")
        {
            riffQuest.gameObject.SetActive(true);
        }

        if (e.missionId == "findArmedRig")
        {
            armedRigQuest.gameObject.SetActive(true);
        }

        if (e.missionId == "findUnarmedRig")
        {
            unarmedRigQuest.gameObject.SetActive(true);
        }

        if (e.missionId == "approachStructure")
        {
            templeQuest.gameObject.SetActive(true);
        }

        if (e.missionId is "photographWhale" or "photographTemple" or "photographRig" or "photographArmedRig")
        {
            DialogueManager.Instance.StartDialogue("Photograph");
        }

        if (e.missionId is "armedRig")
        {
            DialogueManager.Instance.AddMissionServerRpc("turret");
        }
    }

    private void OnMissionFinished(object sender, DialogueManager.OnMissionStartEventArgs e)
    {
        Debug.Log("Finished " + e.missionId);
    }

    public void OnRiffEnter()
    {
        if (!IsHost) return;

        DialogueManager.Instance.RemoveMissionServerRpc("coralreef");
        DialogueManager.Instance.AddMissionServerRpc("destroyCoralreef");
    }

    public void OnRigEnter()
    {
        if (!IsHost) return;

        DialogueManager.Instance.RemoveMissionServerRpc("findUnarmedRig");
        DialogueManager.Instance.StartDialogue("DestroyOilRig");
    }

    public void OnArmedRigEnter()
    {
        if (!IsHost) return;

        DialogueManager.Instance.RemoveMissionServerRpc("findArmedRig");
        DialogueManager.Instance.StartDialogue("ApproachingDefendedRig");
    }

    public void OnTempleEnter()
    {
        if (!IsHost) return;

        DialogueManager.Instance.RemoveMissionServerRpc("approachStructure");
        DialogueManager.Instance.AddMissionServerRpc("photographTemple");
        // TODO treasure
    }

    public void OnWhaleEnter()
    {
        if (!IsHost) return;

        DialogueManager.Instance.StartDialogue("WhaleSighting");
    }

    public void OnWhalePhotographed()
    {
        DialogueManager.Instance.RemoveMissionServerRpc("photographWhale");
    }

    public void OnTemplePhotographed()
    {
        DialogueManager.Instance.RemoveMissionServerRpc("photographTemple");
        DialogueManager.Instance.StartDialogue("TemplePhoto");
    }

    public void OnRigPhotographed()
    {
        DialogueManager.Instance.RemoveMissionServerRpc("photographRig");
        DialogueManager.Instance.StartDialogue("EvidenceSentToReporter");
    }

    public void OnArmedRigPhotographed()
    {
        DialogueManager.Instance.RemoveMissionServerRpc("photographArmedRig");
        DialogueManager.Instance.StartDialogue("DataSecured");
    }
}