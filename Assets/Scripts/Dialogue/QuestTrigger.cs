using System;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

public class QuestTrigger : MonoBehaviour
{
    [SerializeField] private float radius = 200f;
    [SerializeField] private bool doDisable = true;
    [SerializeField] UnityEvent onTrigger;

    private SphereCollider _collider;

    private void OnEnable()
    {
        _collider = GetComponent<SphereCollider>();
        if (_collider == null)
        {
            _collider = gameObject.AddComponent<SphereCollider>();
        }

        _collider.radius = radius;
        _collider.isTrigger = true;
    }

    private void OnDisable()
    {
        Destroy(GetComponent<SphereCollider>());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Submarine"))
        {
            Debug.Log("Submarine entered Trigger " + gameObject.name);
            onTrigger?.Invoke();
            _collider.enabled = false;
            if (doDisable)
                Invoke(nameof(Disable), 10f);
        }
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}