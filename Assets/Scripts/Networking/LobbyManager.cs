using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UI;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Networking
{
    public class LobbyManager : MonoBehaviour
    {
        public static LobbyManager Instance { get; private set; }
        public const string PlayerNameKey = "PlayerName";
        public const string PlayerRoleKey = "PlayerRole";
        public const string LobbyStartGameKey = "StartGame";


        private Lobby _joinedLobby;
        private float _heartbeatTimer, _lobbyUpdateTimer;
        private int _currentPlayerIndex;
        private bool _startedGame;

        public event EventHandler<OnPlayerUpdateEventArgs> OnPlayersUpdate;
        public event EventHandler OnGameStarted;

        static SemaphoreSlim semaphore = new SemaphoreSlim(1, 1);


        public class OnPlayerUpdateEventArgs : EventArgs
        {
            public List<Player> players;
        }

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(this);
            else
                Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        public async void Start()
        {
            await RelayManager.Instance.Initialize();
        }

        public int GetNumPlayers()
        {
            return _joinedLobby.Players.Count;
        }

        public List<Player> GetLobbyPlayers()
        {
            return _joinedLobby.Players;
        }

        public String GetLobbyCode()
        {
            return _joinedLobby.LobbyCode;
        }

        private void Update()
        {
            HandleLobbyHeartbeat();
            HandleLobbyPollForUpdates();
        }

        public bool IsLobbyHost()
        {
            if (_joinedLobby == null) return false;
            return _joinedLobby.HostId == AuthenticationService.Instance.PlayerId;
        }

        public bool IsPlayerInLobby()
        {
            if (_joinedLobby == null) return false;

            foreach (var player in _joinedLobby.Players)
            {
                if (player.Id == AuthenticationService.Instance.PlayerId) return true;
            }

            return false;
        }

        private async void HandleLobbyHeartbeat()
        {
            if (!IsLobbyHost()) return;

            _heartbeatTimer -= Time.deltaTime;
            if (_heartbeatTimer < 0f)
            {
                _heartbeatTimer = 15;

                // lobby closes after 30 without heartbeats
                await LobbyService.Instance.SendHeartbeatPingAsync(_joinedLobby.Id);
            }
        }

        // ReSharper disable Unity.PerformanceAnalysis
        private async void HandleLobbyPollForUpdates()
        {
            if (_joinedLobby == null) return;

            _lobbyUpdateTimer -= Time.deltaTime;
            if (_lobbyUpdateTimer < 0f)
            {
                _lobbyUpdateTimer = 1.1f;
                await semaphore.WaitAsync();

                try
                {
                    // update Lobby
                    _joinedLobby = await LobbyService.Instance.GetLobbyAsync(_joinedLobby.Id);
                    OnPlayersUpdate?.Invoke(this, new OnPlayerUpdateEventArgs {players = _joinedLobby.Players});
                }
                catch (LobbyServiceException e)
                {
                    Debug.LogError(e);
                }
                finally
                {
                    semaphore.Release();
                }

                if (!IsPlayerInLobby())
                {
                    // TODO test
                    _joinedLobby = null;
                    Debug.Log("Leaving");
                    FindAnyObjectByType<MenuScript>().BackButtonPressed(1);
                }

                if (_joinedLobby != null && _joinedLobby.Data[LobbyStartGameKey].Value != "0")
                {
                    if (!IsLobbyHost() && !_startedGame)
                    {
                        _startedGame = true;
                        RelayManager.Instance.JoinRelay(_joinedLobby.Data[LobbyStartGameKey].Value);
                    }

                    OnGameStarted?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public async Task CreateLobby()
        {
            string lobbyName = "Lobby " + Random.Range(0, 1000);
            int maxPlayers = 4;
            Debug.Log("Creating Lobby " + lobbyName);
            try
            {
                var createLobbyOptions = new CreateLobbyOptions
                {
                    IsPrivate = false,
                    Player = GetPlayer(true),
                    Data = new Dictionary<string, DataObject>
                    {
                        {LobbyStartGameKey, new DataObject(DataObject.VisibilityOptions.Member, "0")}
                    }
                };

                _joinedLobby = await LobbyService.Instance.CreateLobbyAsync(lobbyName, maxPlayers, createLobbyOptions);
                _currentPlayerIndex = 0;
                OnPlayersUpdate?.Invoke(this, new OnPlayerUpdateEventArgs {players = _joinedLobby.Players});
                Debug.Log("Created Lobby! " + _joinedLobby.Name + " " + _joinedLobby.Id + " " + _joinedLobby.LobbyCode);
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError(e);
            }
        }

        private Player GetPlayer(bool isHost)
        {
            string pName = NameReader.Instance.GetRandomName();
            RoleController.Role role = isHost ? RoleController.Role.Captain : RoleController.Role.Crewmember;
            var roleString = ((int) role).ToString();

            return new Player
            {
                Data = new Dictionary<string, PlayerDataObject>
                {
                    {PlayerNameKey, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, pName)},
                    {PlayerRoleKey, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, roleString)}
                }
            };
        }

        public async void UpdatePlayerRole(RoleController.Role role)
        {
            var data = _joinedLobby.Players[_currentPlayerIndex].Data;
            data[PlayerRoleKey] =
                new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, ((int) role).ToString());

            await UpdatePlayerData(data);
        }

        private async Task UpdatePlayerData(Dictionary<string, PlayerDataObject> data)
        {
            await semaphore.WaitAsync();
            try
            {
                await Lobbies.Instance.UpdatePlayerAsync(_joinedLobby.Id, AuthenticationService.Instance.PlayerId,
                    new UpdatePlayerOptions
                    {
                        Data = data
                    });
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError(e);
            }
            finally
            {
                semaphore.Release();
            }
        }

        public async void UpdatePlayerName(String playerName)
        {
            var data = _joinedLobby.Players[_currentPlayerIndex].Data;
            data[PlayerNameKey] = new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, playerName);

            await UpdatePlayerData(data);
        }

        public async Task<bool> JoinLobby(string lobbyCode)
        {
            try
            {
                var joinLobbyOptions = new JoinLobbyByCodeOptions()
                {
                    Player = GetPlayer(false)
                };

                _joinedLobby = await Lobbies.Instance.JoinLobbyByCodeAsync(lobbyCode, joinLobbyOptions);
                _currentPlayerIndex = _joinedLobby.Players.Count - 1;
                OnPlayersUpdate?.Invoke(this, new OnPlayerUpdateEventArgs {players = _joinedLobby.Players});
                Debug.Log("Joined Lobby with code " + lobbyCode);
                return true;
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError(e);
            }

            return false;
        }

        public async Task<bool> QuickJoinLobby()
        {
            Debug.Log("QuickJoinLobby");

            var joinLobbyOptions = new QuickJoinLobbyOptions()
            {
                Player = GetPlayer(false)
            };
            try
            {
                _joinedLobby = await Lobbies.Instance.QuickJoinLobbyAsync(joinLobbyOptions);
                _currentPlayerIndex = _joinedLobby.Players.Count - 1;
                OnPlayersUpdate?.Invoke(this, new OnPlayerUpdateEventArgs {players = _joinedLobby.Players});
                Debug.Log("Quick Joined Lobby ");
                return true;
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError(e);
            }

            return false;
        }


        public async void StartGame()
        {
            if (!IsLobbyHost()) return;

            try
            {
                string relayCode = await RelayManager.Instance.CreateRelay();

                _joinedLobby = await Lobbies.Instance.UpdateLobbyAsync(_joinedLobby.Id, new UpdateLobbyOptions
                {
                    Data = new Dictionary<string, DataObject>
                    {
                        {LobbyStartGameKey, new DataObject(DataObject.VisibilityOptions.Member, relayCode)}
                    }
                });
                Loader.Load(Loader.Scene.SampleScene);
            }
            catch (LobbyServiceException e)
            {
                Debug.LogError(e);
            }
        }

        public async void LeaveLobby()
        {
            try
            {
                string playerId = AuthenticationService.Instance.PlayerId;
                await LobbyService.Instance.RemovePlayerAsync(_joinedLobby.Id, playerId);
                _joinedLobby = null;
            }
            catch (LobbyServiceException e)
            {
                Debug.Log(e);
            }
        }
    }
}