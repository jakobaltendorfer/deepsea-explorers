using System;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;

namespace Networking
{
    public class CustomNetworkManager : MonoBehaviour
    {
        public static CustomNetworkManager Instance { get; private set; }

        [SerializeField] private bool debug;
        
        private bool _useRelay;

        public List<Transform> Players { get; } = new List<Transform>();

        public event EventHandler<OnClientConnectedEventArgs> OnClientConnected;
        public event EventHandler<OnPlayerSpawnedEventArgs> OnPlayerSpawned;

        public class OnClientConnectedEventArgs : EventArgs
        {
            public ulong clientId;
        }

        public class OnPlayerSpawnedEventArgs : EventArgs
        {
            public PlayerController player;
        }

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(this);
            else
                Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        private async void Start()
        {
            _useRelay = GetComponent<UnityTransport>().Protocol == UnityTransport.ProtocolType.RelayUnityTransport;
            if (_useRelay && LobbyManager.Instance == null)
                await RelayManager.Instance.Initialize();

            NetworkManager.Singleton.OnClientConnectedCallback += OnClientStarted;
        }

        private void OnClientStarted(ulong clientId)
        {
            OnClientConnected?.Invoke(this, new OnClientConnectedEventArgs
            {
                clientId = clientId
            });
        }

        private void OnGUI()
        {
            if (!debug) return;
            
            GUILayout.BeginArea(new Rect(10, 10, 300, 300));
            if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer)
            {
                StartButtons();
            }
            else
            {
                StatusLabels();
            }

            GUILayout.EndArea();
        }

        public void OnPlayerSpawn(PlayerController player)
        {
            Players.Add(player.transform);
            OnPlayerSpawned?.Invoke(this, new OnPlayerSpawnedEventArgs {player = player});
        }

        private void StartButtons()
        {
            if (GUILayout.Button(_useRelay ? "Host Relay" : "Host")) StartHost();
            if (!_useRelay)
            {
                if (GUILayout.Button("Client"))
                    StartClient();
            }
        }

        // ReSharper disable Unity.PerformanceAnalysis
        private async void StartHost()
        {
            if (_useRelay)
                await RelayManager.Instance.CreateRelay();
            else
                NetworkManager.Singleton.StartHost();
        }

        // ReSharper disable Unity.PerformanceAnalysis
        private void StartClient()
        {
            NetworkManager.Singleton.StartClient();
        }

        private static void StatusLabels()
        {
            var mode = NetworkManager.Singleton.IsHost ? "Host" :
                NetworkManager.Singleton.IsServer ? "Server" : "Client";

            GUILayout.Label("Transport: " +
                            NetworkManager.Singleton.NetworkConfig.NetworkTransport.GetType().Name);
            GUILayout.Label("Mode: " + mode);
        }
    }
}