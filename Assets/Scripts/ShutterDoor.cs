using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShutterDoor : MonoBehaviour
{
    public bool requestStateChange;
    [SerializeField] Animator animator;
    [SerializeField] private List<Light> lights;
    private bool _isOpen = true;

    void Start()
    {
        if (_isOpen)
        {
            animator.SetTrigger("D-Close");
            _isOpen = false;
        }
    }
    void Update()
    {
        if (!requestStateChange) return;

        animator.SetTrigger(!_isOpen ? "D-Open" : "D-Close");
        Color newCol = _isOpen ? Color.red : Color.green;
        foreach (Light l in lights)
        {
            l.color = newCol;
        }
        _isOpen = !_isOpen;
        requestStateChange = false;
    }
}