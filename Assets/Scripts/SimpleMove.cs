using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMove : MonoBehaviour
{
    [SerializeField] private Vector3 movement;

    [SerializeField] private float killTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (killTime > 0f)
            Destroy(gameObject, killTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += movement * Time.deltaTime;
    }
}