using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameInput : MonoBehaviour
{
    public static GameInput Instance { get; private set; }

    public event EventHandler OnInteractAction;
    public event EventHandler OnNextDialogueAction;
    public event EventHandler OnExitAction;
    public event EventHandler OnRotatePressedAction;
    public event EventHandler OnRotateReleasedAction;
    public event EventHandler<OnMouseScrollYActionEventArgs> OnMouseScrollYAction;
    public event EventHandler OnPhotoAction;
    public event EventHandler OnShootAction;
    public event EventHandler OnPauseAction;

    public class OnMouseScrollYActionEventArgs : EventArgs
    {
        public float value;
    }

    private PlayerInputActions _playerInputActions;
    private bool _disabled;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;

        _playerInputActions = new PlayerInputActions();
        _playerInputActions.Player.Enable();

        _playerInputActions.Player.Interact.performed += InteractPerformed;
        _playerInputActions.Player.NextDialogue.performed += NextDialoguePerformed;
        _playerInputActions.Player.Exit.performed += ExitPerformed;
        _playerInputActions.Player.MouseScrollY.performed += MouseScrollYPerformed;
        _playerInputActions.Player.RotatePressed.performed += RotatePressedPerformed;
        _playerInputActions.Player.RotateReleased.performed += RotateReleasedPerformed;
        _playerInputActions.Player.Shoot.performed += ShootPerformed;
        _playerInputActions.Player.Photo.performed += PhotoPerformed;
        _playerInputActions.Player.Pause.performed += PausePerformed;
    }

    public void Disable()
    {
        _disabled = true;
        OnRotateReleasedAction?.Invoke(this, EventArgs.Empty);
    }

    public void Enable()
    {
        _disabled = false;
    }

    private void PausePerformed(InputAction.CallbackContext obj)
    {
        OnPauseAction?.Invoke(this, EventArgs.Empty);
    }

    private void RotatePressedPerformed(InputAction.CallbackContext obj)
    {
        if (!_disabled)
            OnRotatePressedAction?.Invoke(this, EventArgs.Empty);
    }

    private void RotateReleasedPerformed(InputAction.CallbackContext obj)
    {
        if (!_disabled)
            OnRotateReleasedAction?.Invoke(this, EventArgs.Empty);
    }

    private void ShootPerformed(InputAction.CallbackContext obj)
    {
        if (!_disabled)
            OnShootAction?.Invoke(this, EventArgs.Empty);
    }

    private void InteractPerformed(InputAction.CallbackContext obj)
    {
        if (!_disabled)
            OnInteractAction?.Invoke(this, EventArgs.Empty);
    }

    private void NextDialoguePerformed(InputAction.CallbackContext obj)
    {
        OnNextDialogueAction?.Invoke(this, EventArgs.Empty);
    }

    private void ExitPerformed(InputAction.CallbackContext obj)
    {
        if (!_disabled)
            OnExitAction?.Invoke(this, EventArgs.Empty);
    }

    private void MouseScrollYPerformed(InputAction.CallbackContext obj)
    {
        if (!_disabled)
            OnMouseScrollYAction?.Invoke(this, new OnMouseScrollYActionEventArgs {value = obj.ReadValue<float>()});
    }

    private void PhotoPerformed(InputAction.CallbackContext obj)
    {
        if (!_disabled)
            OnPhotoAction?.Invoke(this, EventArgs.Empty);
    }

    public Vector2 GetMovementVector()
    {
        if (_disabled) return Vector2.zero;

        Vector2 inputVector = _playerInputActions.Player.Move.ReadValue<Vector2>();

        inputVector = inputVector.normalized;
        return inputVector;
    }


    public float GetAscendAxis()
    {
        if (_disabled) return 0f;

        return _playerInputActions.Player.Ascend.ReadValue<float>();
    }

    public bool IsShootPressed()
    {
        if (_disabled) return false;
        return _playerInputActions.Player.Shoot.ReadValue<float>() > 0.5f;
    }
}