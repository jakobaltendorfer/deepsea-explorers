using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class IntrusionDetection : NetworkBehaviour
{
    [SerializeField] private GameObject samTurret;
    [SerializeField] private List<GameObject> enemySubmarineSpawners;

    private bool _engaged = false;
    
    private void OnTriggerEnter(Collider other)
    {
        if (!IsHost) return;

        if (other.CompareTag("Submarine") && !_engaged)
        {
            _engaged = true;

            // engage sam turret
            if (samTurret != null)
                samTurret.GetComponent<SamTurret>().target = other.transform;

            // engage enemy submarines
            StartCoroutine(SpawnEnemiesDelayed(5f));
            //Destroy(gameObject);
        }
    }

    private IEnumerator SpawnEnemiesDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);

        for (int i = 0; i < enemySubmarineSpawners.Count; i++)
        {
            if (EnemySpawner.Instance.Idle)
            {
                i--;
                yield return new WaitForSeconds(1f);
                continue;
            }

            enemySubmarineSpawners[i].GetComponent<Spawner>().SpawnSubmarine();
            yield return new WaitForSeconds(delay);
        }
    }
}