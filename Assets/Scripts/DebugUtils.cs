using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugUtils
{
    // Sphere with radius of 1
    private static readonly Vector4[] s_UnitSphere = MakeUnitSphere(16);

    private static Vector4[] MakeUnitSphere(int len)
    {
        Debug.Assert(len > 2);
        var v = new Vector4[len * 3];
        for (int i = 0; i < len; i++)
        {
            var f = i / (float) len;
            float c = Mathf.Cos(f * (float) (Math.PI * 2.0));
            float s = Mathf.Sin(f * (float) (Math.PI * 2.0));
            v[0 * len + i] = new Vector4(c, s, 0, 1);
            v[1 * len + i] = new Vector4(0, c, s, 1);
            v[2 * len + i] = new Vector4(s, 0, c, 1);
        }

        return v;
    }

    public static void DrawArc(float startAngle, float endAngle,
        Vector3 position, Quaternion orientation, float radius,
        Color color, bool drawChord = false, bool drawSector = false,
        int arcSegments = 32)
    {
        float arcSpan = Mathf.DeltaAngle(startAngle, endAngle);

        // Since Mathf.DeltaAngle returns a signed angle of the shortest path between two angles, it 
        // is necessary to offset it by 360.0 degrees to get a positive value
        if (arcSpan <= 0)
        {
            arcSpan += 360.0f;
        }

        // angle step is calculated by dividing the arc span by number of approximation segments
        float angleStep = (arcSpan / arcSegments) * Mathf.Deg2Rad;
        float stepOffset = startAngle * Mathf.Deg2Rad;

        // stepStart, stepEnd, lineStart and lineEnd variables are declared outside of the following for loop
        float stepStart = 0.0f;
        float stepEnd = 0.0f;
        Vector3 lineStart = Vector3.zero;
        Vector3 lineEnd = Vector3.zero;

        // arcStart and arcEnd need to be stored to be able to draw segment chord
        Vector3 arcStart = Vector3.zero;
        Vector3 arcEnd = Vector3.zero;

        // arcOrigin represents an origin of a circle which defines the arc
        Vector3 arcOrigin = position;

        for (int i = 0; i < arcSegments; i++)
        {
            // Calculate approximation segment start and end, and offset them by start angle
            stepStart = angleStep * i + stepOffset;
            stepEnd = angleStep * (i + 1) + stepOffset;

            lineStart.x = Mathf.Cos(stepStart);
            lineStart.y = Mathf.Sin(stepStart);
            lineStart.z = 0.0f;

            lineEnd.x = Mathf.Cos(stepEnd);
            lineEnd.y = Mathf.Sin(stepEnd);
            lineEnd.z = 0.0f;

            // Results are multiplied so they match the desired radius
            lineStart *= radius;
            lineEnd *= radius;

            // Results are multiplied by the orientation quaternion to rotate them 
            // since this operation is not commutative, result needs to be
            // reassigned, instead of using multiplication assignment operator (*=)
            lineStart = orientation * lineStart;
            lineEnd = orientation * lineEnd;

            // Results are offset by the desired position/origin 
            lineStart += position;
            lineEnd += position;

            // If this is the first iteration, set the chordStart
            if (i == 0)
            {
                arcStart = lineStart;
            }

            // If this is the last iteration, set the chordEnd
            if (i == arcSegments - 1)
            {
                arcEnd = lineEnd;
            }

            Debug.DrawLine(lineStart, lineEnd, color);
        }

        if (drawChord)
        {
            Debug.DrawLine(arcStart, arcEnd, color);
        }

        if (drawSector)
        {
            Debug.DrawLine(arcStart, arcOrigin, color);
            Debug.DrawLine(arcEnd, arcOrigin, color);
        }
    }

    public static void DrawSphere(Vector4 pos, float radius, Color color)
    {
        Vector4[] v = s_UnitSphere;
        int len = s_UnitSphere.Length / 3;
        for (int i = 0; i < len; i++)
        {
            var sX = pos + radius * v[0 * len + i];
            var eX = pos + radius * v[0 * len + (i + 1) % len];
            var sY = pos + radius * v[1 * len + i];
            var eY = pos + radius * v[1 * len + (i + 1) % len];
            var sZ = pos + radius * v[2 * len + i];
            var eZ = pos + radius * v[2 * len + (i + 1) % len];
            Debug.DrawLine(sX, eX, color);
            Debug.DrawLine(sY, eY, color);
            Debug.DrawLine(sZ, eZ, color);
        }
    }

    public static void DrawCircle(Vector3 position, Quaternion rotation, float radius, Color color, int segments = 32)
    {
        // If either radius or number of segments are less or equal to 0, skip drawing
        if (radius <= 0.0f || segments <= 0)
        {
            return;
        }

        // Single segment of the circle covers (360 / number of segments) degrees
        float angleStep = (360.0f / segments);

        // Result is multiplied by Mathf.Deg2Rad constant which transforms degrees to radians
        // which are required by Unity's Mathf class trigonometry methods

        angleStep *= Mathf.Deg2Rad;

        // lineStart and lineEnd variables are declared outside of the following for loop
        Vector3 lineStart = Vector3.zero;
        Vector3 lineEnd = Vector3.zero;

        for (int i = 0; i < segments; i++)
        {
            // Line start is defined as starting angle of the current segment (i)
            lineStart.x = Mathf.Cos(angleStep * i);
            lineStart.y = Mathf.Sin(angleStep * i);
            lineStart.z = 0.0f;

            // Line end is defined by the angle of the next segment (i+1)
            lineEnd.x = Mathf.Cos(angleStep * (i + 1));
            lineEnd.y = Mathf.Sin(angleStep * (i + 1));
            lineEnd.z = 0.0f;

            // Results are multiplied so they match the desired radius
            lineStart *= radius;
            lineEnd *= radius;

            // Results are multiplied by the rotation quaternion to rotate them 
            // since this operation is not commutative, result needs to be
            // reassigned, instead of using multiplication assignment operator (*=)
            lineStart = rotation * lineStart;
            lineEnd = rotation * lineEnd;

            // Results are offset by the desired position/origin 
            lineStart += position;
            lineEnd += position;

            // Points are connected using DrawLine method and using the passed color
            Debug.DrawLine(lineStart, lineEnd, color);
        }
    }

    public static void DrawCylinder(Vector3 position, Quaternion orientation, float height, float radius, Color color,
        bool drawFromBase = true)
    {
        Vector3 localUp = orientation * Vector3.up;
        Vector3 localRight = orientation * Vector3.right;
        Vector3 localForward = orientation * Vector3.forward;

        Vector3 basePositionOffset = drawFromBase ? Vector3.zero : (localUp * height * 0.5f);
        Vector3 basePosition = position - basePositionOffset;
        Vector3 topPosition = basePosition + localUp * height;

        Quaternion circleOrientation = orientation * Quaternion.Euler(90, 0, 0);

        Vector3 pointA = basePosition + localRight * radius;
        Vector3 pointB = basePosition + localForward * radius;
        Vector3 pointC = basePosition - localRight * radius;
        Vector3 pointD = basePosition - localForward * radius;

        Debug.DrawRay(pointA, localUp * height, color);
        Debug.DrawRay(pointB, localUp * height, color);
        Debug.DrawRay(pointC, localUp * height, color);
        Debug.DrawRay(pointD, localUp * height, color);

        DrawCircle(basePosition, circleOrientation, radius, color);
        DrawCircle(topPosition, circleOrientation, radius, color);
    }

    public static void DrawCapsule(Vector3 position, Quaternion orientation, float height, float radius, Color color,
        bool drawFromBase = true)
    {
        // Clamp the radius to a half of the capsule's height
        radius = Mathf.Clamp(radius, 0, height * 0.5f);
        Vector3 localUp = orientation * Vector3.up;
        Quaternion arcOrientation = orientation * Quaternion.Euler(0, 90, 0);

        Vector3 basePositionOffset = drawFromBase ? Vector3.zero : (localUp * (height * 0.5f));
        Vector3 baseArcPosition = position + localUp * radius - basePositionOffset;
        DrawArc(180, 360, baseArcPosition, orientation, radius, color);
        DrawArc(180, 360, baseArcPosition, arcOrientation, radius, color);

        float cylinderHeight = height - radius * 2.0f;
        DrawCylinder(baseArcPosition, orientation, cylinderHeight, radius, color, true);

        Vector3 topArcPosition = baseArcPosition + localUp * cylinderHeight;

        DrawArc(0, 180, topArcPosition, orientation, radius, color);
        DrawArc(0, 180, topArcPosition, arcOrientation, radius, color);
    }
}