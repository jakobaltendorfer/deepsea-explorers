using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.VFX;

public class OilDerrick : NetworkBehaviour
{
    [SerializeField] private bool armed;
    [SerializeField] private List<Transform> destroyedImpacts;
    private bool _isExploded = false; // Add a flag to prevent multiple explosions

    public void TakeDamage(int damage)
    {
        TakeDamageServerRpc(damage);
    }

    [ServerRpc(RequireOwnership = false)]
    private void TakeDamageServerRpc(int damage)
    {
        OilRig oilRig = transform.parent.GetComponent<OilRig>();
        oilRig.ChangeHealth(-damage);

        if (!_isExploded && oilRig.Health < 0)
        {
            if (armed)
            {
                DialogueManager.Instance.AddMissionServerRpc("photographArmedRig");
                DialogueManager.Instance.RemoveMissionServerRpc("armedRig");
            }
            else
            {
                DialogueManager.Instance.AddMissionServerRpc("photographRig");
                DialogueManager.Instance.RemoveMissionServerRpc("unarmedRig");
            }

            _isExploded = true; // Set the flag to prevent multiple explosions
            ExplodeClientRpc();
        }
    }

    [ClientRpc]
    private void ExplodeClientRpc()
    {
        StartCoroutine(EnableVisualEffectsWithDelay());
    }

    private IEnumerator EnableVisualEffectsWithDelay()
    {
        foreach (Transform t in destroyedImpacts)
        {
            t.gameObject.SetActive(true);

            // Wait for a specified delay before proceeding to the next VFX
            yield return new WaitForSeconds(.2f); // Adjust the delay time as needed

            // Destroy the GameObject after a certain time
            Destroy(t.gameObject, 3f); // You can adjust the destruction time as needed
        }
    }
}