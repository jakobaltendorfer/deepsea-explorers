using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class OilRig : NetworkBehaviour
{
    [SerializeField] private GameObject oilDerrick;
    [SerializeField] private GameObject indicator;
    [SerializeField] private float maxHealth = 100f;

    public float _health;

    private Animator _oilDerrickAnimator;

    public float Health => _health;

    // Start is called before the first frame update
    void Start()
    {
    }

    public override void OnNetworkSpawn()
    {
        if (!IsHost) return;

        _health = maxHealth;
        UpdateHealthClientRpc(_health);
    }

    public void ChangeHealth(float value)
    {
        _health += value;

        if (_health <= 0)
        {
            StopAnimationServerRpc();
        }

        Debug.Log(_health);

        UpdateHealthClientRpc(_health);
    }

    [ServerRpc(RequireOwnership = false)]
    private void StopAnimationServerRpc()
    {
        StopAnimationClientRpc();
    }

    [ClientRpc]
    private void StopAnimationClientRpc()
    {
        oilDerrick.GetComponent<Animator>().enabled = false;
    }

    [ClientRpc]
    private void UpdateHealthClientRpc(float health)
    {
        var cubeRenderer = indicator.GetComponent<Renderer>();

        // Calculate emission color based on health (for example, from red to green)
        Color newEmissionColor = Color.Lerp(Color.red, Color.green, health / maxHealth);

        // Set the emission color of the material
        cubeRenderer.material.SetColor("_EmissionColor", newEmissionColor);

        // Enable emission on the material
        cubeRenderer.material.EnableKeyword("_EMISSION");
    }
}