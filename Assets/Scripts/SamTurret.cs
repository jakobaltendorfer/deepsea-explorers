using System;
using System.Collections;
using System.Collections.Generic;
using Networking;
using NUnit.Framework.Constraints;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;
using Unity.VisualScripting;


public class SamTurret : NetworkBehaviour
{
    [SerializeField] private Transform guns;
    [SerializeField] private Transform baseTransfrom;
    [SerializeField] private GameObject rocket;
    [SerializeField] private List<Transform> chambers;
    [SerializeField] private bool shoot;
    [SerializeField] private float rotateSpeed = 2f;
    [SerializeField] private float radarRotateSpeed = 5f;
    [SerializeField] private Transform radar;
    [SerializeField] private Material samTurretIndicator;
    [SerializeField] private float fireFrequency = 4f;
    [SerializeField] private GameObject explosion;
    private Vector3 _targetPosition;
    [SerializeField] private float _timeWithoutFiring;

    public Transform target;
    public float health = 100f;
    private bool _destroyed = false;


    void Update()
    {
        if (!IsHost) return;

        if (_destroyed || EnemySpawner.Instance.Idle)
        {
            return;
        }

        RotateRadar();

        if (!target)
        {
            SetIndicatorColorClientRpc(Color.green);
            AssumeDisarmedPosition();
            return;
        }


        _targetPosition = target.position;
        SetIndicatorColorClientRpc(Color.red);
        HorizontalRotation();
        VerticalRotation();
        _timeWithoutFiring += Time.deltaTime;

        if (shoot || (_timeWithoutFiring >= fireFrequency))
        {
            Shoot();
            _timeWithoutFiring = 0f;
            shoot = false;
        }
    }

    [ClientRpc]
    private void SetIndicatorColorClientRpc(Color color)
    {
        samTurretIndicator.SetColor("_EmissionColor", color);
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        GetComponent<Rigidbody>().isKinematic = true;
    }

    public void TakeDamage(int damage)
    {
        TakeDamageServerRpc(damage);
    }

    [ServerRpc(RequireOwnership = false)]
    public void TakeDamageServerRpc(int damage)
    {
        health -= damage;

        if (health <= 0 && !_destroyed)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        _destroyed = true;

        var explosionGo = Instantiate(explosion, transform.position, Quaternion.identity);
        explosionGo.GetComponent<NetworkObject>().Spawn();
        explosionGo.transform.localScale = Vector3.one * 3;
        Destroy(explosionGo, 1f);

        // destruct
        DestroyClientRpc();

        // remove missin
        DialogueManager.Instance.RemoveMissionServerRpc("turret");
    }

    [ClientRpc]
    private void DestroyClientRpc()
    {
        GetComponent<AudioSource>().Play();
        foreach (Transform g in transform.GetChild(1).GetChild(0).GetComponentsInChildren<Transform>())
        {
            Rigidbody rbChild = g.AddComponent<Rigidbody>();
            rbChild.drag = 2f;
            rbChild.angularDrag = 1f;
            rbChild.AddExplosionForce(5000f, transform.position, 13f);
        }
    }

    private void AssumeDisarmedPosition()
    {
        Vector3 ground = new Vector3(0, -120, 0); // Set x and z to 0, and y to -120
        Vector3 relativePos = ground - guns.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        rotation = Quaternion.Euler(0, -rotation.eulerAngles.x - 90f, 0);
        guns.localRotation = Quaternion.RotateTowards(guns.localRotation, rotation, rotateSpeed * Time.deltaTime);
    }

    private void RotateRadar()
    {
        radar.Rotate(Vector3.right * (radarRotateSpeed * Time.deltaTime));
    }

    private void VerticalRotation()
    {
        // rotate on z-axis
        Vector3 relativePos = _targetPosition - guns.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        rotation = Quaternion.Euler(0, -rotation.eulerAngles.x - 30f, 0);
        guns.localRotation = Quaternion.RotateTowards(guns.localRotation, rotation, rotateSpeed * Time.deltaTime);
    }

    private void HorizontalRotation()
    {
        // rotate on y-axis
        Vector3 relativePos = _targetPosition - baseTransfrom.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        rotation = Quaternion.Euler(-rotation.eulerAngles.y + 90f, 0, 0);
        baseTransfrom.localRotation =
            Quaternion.RotateTowards(baseTransfrom.localRotation, rotation, rotateSpeed * Time.deltaTime);
    }

    private void Shoot()
    {
        Transform chamber = chambers[Random.Range(0, 7)];
        var projectileNO =
            NetworkObjectPool.Singleton.GetNetworkObject(rocket, chamber.position, chamber.rotation);

        Vector3 newRotation = projectileNO.transform.rotation.eulerAngles;
        newRotation.x += 210f;
        projectileNO.transform.rotation = Quaternion.Euler(newRotation);
        projectileNO.transform.localScale = Vector3.one * 2;

        HomingRocket homingRocket = projectileNO.GetComponent<HomingRocket>();
        homingRocket.target = target.position;
        homingRocket.activated = true;
        homingRocket.rocketPrefab = rocket;

        projectileNO.Spawn();
    }
}