using System;
using System.Collections;
using System.Collections.Generic;
using Castle.Components.DictionaryAdapter;
using Networking;
using Submarine;
using Unity.Netcode;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : NetworkBehaviour
{
    public static EnemySpawner Instance { get; private set; }

    [SerializeField] private GameObject enemy;
    [SerializeField] private float spawnDistance = 200f;
    [SerializeField] private int spawnCountPerPlayer = 4;
    [SerializeField] private float randomHeight = 3f;
    [SerializeField] private AnimationCurve animationCurve;
    [SerializeField] private float overallSpawnTime = 30f;

    private List<GameObject> _spawnedEnemies = new List<GameObject>();
    private bool _finishedSpawning = true;
    private bool _idle = false;

    public bool Idle => _idle;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    public void Spawn(float delay)
    {
        if (!IsHost) return;

        StartCoroutine(SpawnDelayed(delay));
    }

    public void AddChildToList(GameObject enemySubmarine)
    {
        _spawnedEnemies.Add(enemySubmarine);
    }

    public void SetEnemiesToIdle(bool isIdle)
    {
        _idle = isIdle;
    }

    public void CheckFleetDestruction()
    {
        if (!IsHost) return;

        bool currentFleetDestroyed = true;
        foreach (GameObject enemySubmarine in _spawnedEnemies)
        {
            print("Here");
            if (enemySubmarine.GetComponent<EnemySubmarine>().state != EnemySubmarine.EnemyState.Destroyed)
            {
                currentFleetDestroyed = false;
                break;
            }
        }

        if (_finishedSpawning && currentFleetDestroyed)
        {
            Invoke(nameof(AllEnemiesDefeated), 2f);
        }
    }

    private void AllEnemiesDefeated()
    {
        print("Complete Fleet destroyed");
        DialogueManager.Instance.RemoveMissionServerRpc("subBattle");
        DialogueManager.Instance.StartDialogue("AllEnemiesDefeated");

        AudioManager.Instance.StopDangerous();
    }

    private IEnumerator SpawnDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);

        _finishedSpawning = false;
        AudioManager.Instance.PlayDangerous();

        int spawnCount = LobbyManager.Instance != null
            ? spawnCountPerPlayer * LobbyManager.Instance.GetNumPlayers()
            : spawnCountPerPlayer;

        var subPosition = SubmarineController.Instance.transform.position;
        for (int i = 0; i < spawnCount; i++)
        {
            if (_idle)
            {
                i--;
                yield return new WaitForSeconds(1f);
                continue;
            }

            var offset = Random.insideUnitCircle.normalized * spawnDistance;
            var position = subPosition + new Vector3(offset.x, Random.Range(-randomHeight, randomHeight), offset.y);
            float terrainHeight = TerrainController.Instance.SampleTerrainHeight(position.x, position.z);
            position.y = Mathf.Max(position.y, terrainHeight + 2f);

            var go = Instantiate(enemy, position, Quaternion.identity);
            go.transform.forward = (subPosition - position).normalized;
            go.GetComponent<NetworkObject>().Spawn();
            _spawnedEnemies.Add(go);

            float time1 = animationCurve.Evaluate(i / (float) (spawnCount - 1)) * overallSpawnTime;
            float time2 = animationCurve.Evaluate((i + 1) / (float) (spawnCount - 1)) * overallSpawnTime;
            float sleepTime = time2 - time1;

            yield return new WaitForSeconds(sleepTime);
        }

        _finishedSpawning = true;
    }
}