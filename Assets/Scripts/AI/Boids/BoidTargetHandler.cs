using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidTargetHandler : MonoBehaviour
{
    [SerializeField] private GameObject boidTargetFar;
    [SerializeField] private GameObject boidTargetClose;
    [SerializeField] private float switchTime = 20f;

    private float _timer;

    // Start is called before the first frame update
    void Start()
    {
        boidTargetFar.SetActive(false);
        boidTargetClose.SetActive(true);

        _timer = switchTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (_timer > 0f)
        {
            _timer -= Time.deltaTime;
            return;
        }

        SwitchTargets();
    }

    void SwitchTargets()
    {
        boidTargetFar.SetActive(!boidTargetFar.activeSelf);
        boidTargetClose.SetActive(!boidTargetClose.activeSelf);
        _timer = switchTime;
    }
}