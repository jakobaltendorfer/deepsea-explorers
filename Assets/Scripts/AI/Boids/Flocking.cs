using System;
using System.Collections.Generic;
using Submarine;
using Unity.Profiling;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AI.Boids
{
    public class Flocking : MonoBehaviour
    {
        [SerializeField] private List<Transform> prefabs;
        [SerializeField] private BoidSettings settings;
        [SerializeField] private uint numInstances = 100;
        [SerializeField] private float spawnDistance = 100.0f;
        [SerializeField] private bool spawnAtStart = false;
        [SerializeField] private bool debug = false;

        private readonly List<Boid> _boids = new List<Boid>();
        private bool _isSpawned;
        private Camera _camera;

        private void Start()
        {
            _camera = Camera.main;
            Random.InitState(0);
            if (spawnAtStart)
                SpawnBoids();
        }


        // ReSharper disable Unity.PerformanceAnalysis
        void SpawnBoids()
        {
            if (prefabs.Count == 0) return;

            var cachedTransform = transform;
            var scale = cachedTransform.localScale;
            var halfScale = scale / 2 - Vector3.one * 2; // Vector.one for small padding

            for (int i = 0; i < numInstances; i++)
            {
                float x = Random.Range(-halfScale.x, halfScale.x);
                float y = Random.Range(-halfScale.y, halfScale.y);
                float z = Random.Range(-halfScale.z, halfScale.z);

                var potentialSpawnPoint = new Vector3(x, y, z) + cachedTransform.position;
                if (Physics.OverlapSphere(potentialSpawnPoint, settings.boundsRadius).Length > 0)
                {
                    i--;
                    continue;
                }

                var prefab = prefabs[Random.Range(0, prefabs.Count)];
                var obj = Instantiate(prefab, potentialSpawnPoint, Random.rotation);
                _boids.Add(obj.GetComponent<Boid>());
            }
        }

        void DespawnBoids()
        {
            foreach (var boid in _boids)
            {
                Destroy(boid.gameObject);
            }

            _boids.Clear();
        }

        void Update()
        {
            float distanceToPlayer = 0f;
            if (!spawnAtStart)
                distanceToPlayer =
                    Vector3.Distance(_camera.transform.position, transform.position);

            if (!_isSpawned && distanceToPlayer <= spawnDistance)
            {
                SpawnBoids();
                _isSpawned = true;
            }
            else if (_isSpawned && distanceToPlayer > spawnDistance)
            {
                DespawnBoids();
                _isSpawned = false;
            }

            if (_isSpawned)
            {
                UpdateBoids();
            }
        }

        (Vector3, Vector3, Vector3) CalculateSeparationAlignmentCohesion(Boid boid)
        {
            if (boid.others.Count == 0)
                return (Vector3.zero, Vector3.zero, Vector3.zero);

            var boidTransform = boid.transform;

            Vector3 separation = Vector3.zero, alignment = Vector3.zero, cohesion = Vector3.zero;
            int count = 0;
            foreach (var other in boid.others)
            {
                var otherTransform = other.transform;
                Vector3 diff = boidTransform.position - otherTransform.position;
                float distance = Mathf.Max(diff.magnitude, 0.001f);


                // Check if other boid is within the cone of vision
                if (Vector3.Dot(boidTransform.forward, diff.normalized) < settings.maxAngle)
                {
                    if (debug)
                        Debug.DrawLine(boidTransform.position, otherTransform.position);
                    count++;

                    separation += diff.normalized * (1f / distance);
                    alignment += other.velocity;
                    cohesion += otherTransform.position;
                }
                else
                {
                    if (debug)
                        Debug.DrawLine(boidTransform.position, otherTransform.position, new Color(0.1f, 0.1f, 0.1f));
                }
            }

            if (count == 0)
                return (Vector3.zero, Vector3.zero, Vector3.zero);


            separation /= count;
            alignment /= count;
            cohesion /= count;

            cohesion -= boidTransform.position;

            return (separation, alignment, cohesion);
        }

        void UpdateBoids()
        {
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var index = 0; index < _boids.Count; index++)
            {
                var boid = _boids[index];
                Vector3 separation, alignment, cohesion;
                (separation, alignment, cohesion) = CalculateSeparationAlignmentCohesion(boid);

                if (debug)
                {
                    Debug.DrawRay(boid.transform.position, separation * settings.seperateWeight, Color.cyan);
                    Debug.DrawRay(boid.transform.position, alignment * settings.alignWeight, Color.yellow);
                    Debug.DrawRay(boid.transform.position, cohesion * settings.cohesionWeight, Color.red);
                }


                Vector3 acc = Vector3.zero;
                acc += SteerTowards(boid, alignment) * settings.alignWeight;
                acc += SteerTowards(boid, cohesion) * settings.cohesionWeight;
                acc += SteerTowards(boid, separation) * settings.seperateWeight;


                float hitDistance;
                bool collides = IsHeadingForCollision(boid, out hitDistance);
                if (collides)
                {
                    Vector3 collisionAvoidDir = ObstacleRays(boid, hitDistance);
                    Vector3 collisionAvoidForce = (collisionAvoidDir - boid.velocity);
                    acc += collisionAvoidForce;
                    if (debug)
                    {
                        var pos = boid.transform.position + boid.transform.forward * hitDistance;
                        DebugUtils.DrawSphere(pos, settings.boundsRadius, Color.red);
                    }
                }

                boid.velocity += acc * Time.deltaTime;
                float speed = boid.velocity.magnitude;
                if (speed == 0) continue;
                // if (debug)
                //     Debug.DrawLine(boid.transform.position, boid.transform.position + boid.velocity, Color.cyan);

                Vector3 dir = boid.velocity / speed;
                speed = Mathf.Clamp(speed, settings.minSpeed, settings.maxSpeed);
                boid.velocity = dir * speed;

                var boidTransform = boid.transform;
                boidTransform.forward = dir;
                boidTransform.position += boid.velocity * (Time.deltaTime * settings.timeScale);
            }
        }

        bool IsHeadingForCollision(Boid boid, out float distance)
        {
            var boidTransform = boid.transform;


            RaycastHit hit;
            bool collides = Physics.SphereCast(boidTransform.position, settings.boundsRadius, boidTransform.forward,
                out hit, settings.collisionAvoidDst, settings.obstacleMask);

            distance = hit.distance;
            return collides;
        }

        Vector3 ObstacleRays(Boid boid, float distance)
        {
            var cachedTransform = boid.transform;
            Vector3[] rayDirections = BoidHelper.Directions;
            var resDir = -cachedTransform.forward;
            for (int i = 0; i < rayDirections.Length; i++)
            {
                Vector3 dir = cachedTransform.TransformDirection(rayDirections[i]);

                Ray ray = new Ray(cachedTransform.position, dir);

                if (!Physics.SphereCast(ray, settings.boundsRadius, settings.collisionAvoidDst, settings.obstacleMask))
                {
                    resDir = dir;
                    break;
                }
            }

            var resDirScaled = resDir / (distance + Mathf.Epsilon) * settings.avoidCollisionWeight;
            if (debug)
                Debug.DrawLine(cachedTransform.position, cachedTransform.position + resDir * settings.collisionAvoidDst,
                    Color.blue);
            return resDirScaled;
        }

        Vector3 SteerTowards(Boid boid, Vector3 vector)
        {
            Vector3 v = vector.normalized * settings.maxSpeed - boid.velocity;
            return Vector3.ClampMagnitude(v, settings.maxSteerForce);
        }


        void OnDrawGizmos()
        {
            DrawBox(new Color(0, 0, 1, 0.4f));
        }

        void OnDrawGizmosSelected()
        {
            DrawBox(new Color(0, 0, 1, 0.8f));
        }

        void DrawBox(Color color)
        {
            Gizmos.color = color;
            var cachedTransform = transform;

            Gizmos.DrawWireCube(cachedTransform.position, cachedTransform.localScale);
        }
    }
}