using System.Collections.Generic;
using UnityEngine;

namespace AI.Boids
{
    public class Boid : MonoBehaviour
    {
        public Vector3 velocity = Vector3.zero;
        public List<Boid> others = new List<Boid>();
        [SerializeField] public int id;

        // Start is called before the first frame update
        void Start()
        {
            velocity = transform.forward;
            float radius = GetComponent<SphereCollider>().radius;

            var colliders = Physics.OverlapSphere(transform.position, radius);
            foreach (var other in colliders)
            {
                if (other.CompareTag("Boid") && other.transform != transform)
                {
                    var boid = other.GetComponent<Boid>();
                    if (boid.id == id)
                        others.Add(boid);
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Boid") && other.transform != transform)
            {
                var boid = other.GetComponent<Boid>();
                if (boid.id == id)
                    others.Add(boid);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Boid"))
            {
                var boid = other.GetComponent<Boid>();
                if (boid.id == id)
                    others.Remove(boid);
            }
        }
    }
}