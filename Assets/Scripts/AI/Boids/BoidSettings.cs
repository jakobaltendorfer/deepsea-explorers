using UnityEngine;


[CreateAssetMenu(fileName = "New Boid Settings", menuName = "Boid Settings", order = 0)]
public class BoidSettings : ScriptableObject
{
    // Settings
    public float minSpeed = 2;
    public float maxSpeed = 5;

    [Range(0.0f, 1f)] [SerializeField]
    public float maxAngle = 0.5f;

    public float maxSteerForce = 3;

    public float alignWeight = 1;
    public float cohesionWeight = 1;
    public float seperateWeight = 1;


    [Header("Collisions")] public LayerMask obstacleMask;
    public float boundsRadius = .27f;
    public float avoidCollisionWeight = 10;
    public float collisionAvoidDst = 5;

    [Range(0f, 1f)] [SerializeField] public float timeScale = 1f;
}