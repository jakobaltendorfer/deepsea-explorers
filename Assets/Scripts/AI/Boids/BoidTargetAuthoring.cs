using Unity.Entities;
using UnityEngine;

namespace Boids
{
    public class BoidTargetAuthoring : MonoBehaviour
    {
        public float TargetWeight;

        class Baker : Baker<BoidTargetAuthoring>
        {
            public override void Bake(BoidTargetAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Renderable);
                AddComponent(entity, new BoidTarget
                {
                    TargetWeight = authoring.TargetWeight
                });
            }
        }
    }

    public struct BoidTarget : IComponentData
    {
        public float TargetWeight;
        public BoidTarget(float targetWeight) => TargetWeight = targetWeight;
    }
}