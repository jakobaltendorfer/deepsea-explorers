using System;
using System.Collections;
using System.Collections.Generic;
using Boids;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class EntityTransformSync : MonoBehaviour
{
    public enum ComponentType
    {
        Target,
        Obstacle
    };

    private EntityManager _manager;

    private Entity _entity;
    public ComponentType componentType;
    public float obststacleRdius = 20f;
    public float targetWeight = 1f;

    private bool _removed = true;

    // Start is called before the first frame update
    void Start()
    {
        _manager = World.DefaultGameObjectInjectionWorld.Unmanaged.EntityManager;
        var archtype = _manager.CreateArchetype(typeof(LocalToWorld), typeof(LocalTransform));
        _entity = _manager.CreateEntity(archtype);

        _manager.AddComponentData(_entity, new LocalToWorld());
        _manager.AddComponentData(_entity, new LocalTransform());
        switch (componentType)
        {
            case ComponentType.Target:
                _manager.AddComponentData(_entity, new BoidTarget(targetWeight));
                break;
            case ComponentType.Obstacle:
                _manager.AddComponentData(_entity, new BoidObstacle(obststacleRdius));
                break;
        }

        _removed = false;
    }

    // Update is called once per frame
    void Update()
    {
        _manager.SetComponentData(_entity, new LocalTransform
        {
            Position = transform.position
        });
    }

    private void OnDisable()
    {
        if (_removed || _entity == Entity.Null) return;

        // Check if the World is valid
        if (World.DefaultGameObjectInjectionWorld == null ||
            World.DefaultGameObjectInjectionWorld.IsCreated == false) return;

        // Check if the Entity exists
        if (!_manager.Exists(_entity)) return;

        _manager.RemoveComponent<BoidTarget>(_entity);
        _removed = true;
    }

    private void OnEnable()
    {
        if (!_removed || _entity == Entity.Null) return;

        // Check if the World is valid
        if (World.DefaultGameObjectInjectionWorld == null ||
            World.DefaultGameObjectInjectionWorld.IsCreated == false) return;

        // Check if the Entity exists
        if (!_manager.Exists(_entity)) return;

        try
        {
            switch (componentType)
            {
                case ComponentType.Target:
                    _manager.AddComponentData(_entity, new BoidTarget(targetWeight));
                    break;
                case ComponentType.Obstacle:
                    _manager.AddComponentData(_entity, new BoidObstacle(obststacleRdius));
                    break;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        _removed = false;
    }
}