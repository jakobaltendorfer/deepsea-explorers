using System;
using Boids;
using Unity.Entities;
using UnityEngine;


namespace Boids
{
    public class BoidObstacleAuthoringBaker : Baker<BoidObstacleAuthoring>
    {
        public override void Bake(BoidObstacleAuthoring authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent(entity, new BoidObstacle
            {
                Radius = authoring.Radius
            });
        }
    }

    public struct BoidObstacle : IComponentData
    {
        public float Radius;

        public BoidObstacle(float radius) => Radius = radius;
    }

    public class BoidObstacleAuthoring : MonoBehaviour
    {
        public float Radius;
    }
}