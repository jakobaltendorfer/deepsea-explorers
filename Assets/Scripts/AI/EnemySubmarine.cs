using System;
using System.Collections;
using System.Collections.Generic;
using Networking;
using Submarine;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class EnemySubmarine : NetworkBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private float rotationSpeed = 50;
    [SerializeField] private float engagementDistance = 30f;
    [SerializeField] private float shootCoolDown = 10f;
    [SerializeField] private float riseSpeed = 2f;
    [SerializeField] private float riseHeight = 50f;
    [SerializeField] private Transform rotor;
    [SerializeField] private float rotorSpeed = 500f;
    [SerializeField] private float bulletSpeed = 40f;
    [SerializeField] private GameObject rocket;
    [SerializeField] private Transform rocketSpawn;
    [SerializeField] private GameObject selfExplosion;
    [SerializeField] private float randomOffsetHeight = 5f;
    [SerializeField] private float collisionDistance = 5f;
    [SerializeField] private LayerMask collisionLayerMask;

    private NetworkVariable<bool> _isKinematic = new NetworkVariable<bool>();


    private float _timeSinceLastShot = 0.0f; // Time since the last shot
    private Vector3 _destination;
    private float _requiredHeight;
    private Rigidbody _rb;
    private Vector3 _targetOffset;

    public float health = 100f;
    public EnemyState state = EnemyState.Closing;

    public enum EnemyState
    {
        Closing,
        Attacking,
        Rise,
        Destroyed,
    }

    void Start()
    {
        //_destination = transform.position + Vector3.up * riseHeight;
        _requiredHeight = transform.position.y + riseHeight;

        _rb = GetComponent<Rigidbody>();
        if (IsHost)
            _isKinematic.Value = true;
        state = EnemyState.Rise;

        Random.InitState(0);
    }

    public override void OnNetworkSpawn()
    {
        var target = SubmarineController.Instance.transform;
        CalculateClosingOffset(target.position);
    }

    public void TakeDamage(int damage)
    {
        TakeDamageServerRpc(damage);
    }

    [ServerRpc(RequireOwnership = false)]
    private void TakeDamageServerRpc(int damage)
    {
        health -= damage;

        CheckHealth();
    }

    private void Die()
    {
        var explosionGO = Instantiate(selfExplosion, transform.position, Quaternion.identity);
        explosionGO.GetComponent<NetworkObject>().Spawn();
        Destroy(explosionGO, 1f);

        state = EnemyState.Destroyed;

        DieClientRpc();

        var intensity = CameraShaker.Instance.IsInDistance(transform.position);
        if (intensity > 0f)
        {
            CameraShaker.Instance.ShakeCamera(6f * intensity, 1f);
        }

        EnemySpawner.Instance.CheckFleetDestruction();
    }

    [ClientRpc]
    private void DieClientRpc()
    {
        GetComponent<AudioSource>().Play();
        Transform model = transform.GetChild(0);
        foreach (Transform child in model)
        {
            Rigidbody rbChild = child.AddComponent<Rigidbody>();
            rbChild.drag = 2f;
            rbChild.angularDrag = 1f;
            rbChild.AddExplosionForce(5000f, transform.position, 10f);
        }

        state = EnemyState.Destroyed;
    }

    void Update()
    {
        if (!IsHost) return;

        if (EnemySpawner.Instance.Idle) return;

        _timeSinceLastShot += Time.deltaTime;
        _rb.isKinematic = _isKinematic.Value;

        if (state == EnemyState.Destroyed)
        {
            return;
        }

        MoveRotor();

        if (state == EnemyState.Rise)
        {
            Rise();
        }

        if (state == EnemyState.Closing)
        {
            var target = SubmarineController.Instance.transform;
            CloseDistance(target.position + _targetOffset);
        }

        if (state == EnemyState.Attacking)
        {
            Attack();
        }
    }

    private static Vector2 PolarToCartesian(float radius, float angleInDegrees)
    {
        float x = radius * Mathf.Cos(Mathf.Deg2Rad * angleInDegrees);
        float y = radius * Mathf.Sin(Mathf.Deg2Rad * angleInDegrees);

        return new Vector2(x, y);
    }

    private static (float radius, float angleInDegrees) CartesianToPolar(Vector2 cartesian)
    {
        float radius = cartesian.magnitude;
        float angleInDegrees = Mathf.Atan2(cartesian.y, cartesian.x) * Mathf.Rad2Deg;

        return (radius, angleInDegrees);
    }

    private void CalculateClosingOffset(Vector3 target)
    {
        var position = transform.position;
        var direction = position - target;
        var direction2d = new Vector2(direction.x, direction.z);
        var (baseRadius, baseAngle) = CartesianToPolar(direction2d);


        var angle = baseAngle + Random.Range(-120, 120);
        var radius = engagementDistance * 0.98f;
        var offset2d = PolarToCartesian(radius, angle);
        _targetOffset = new Vector3(offset2d.x, Random.Range(-randomOffsetHeight, randomOffsetHeight), offset2d.y);
    }

    private void Attack()
    {
        FaceTarget();
        if (_timeSinceLastShot >= shootCoolDown)
        {
            _timeSinceLastShot = 0f;
            Shoot();

            if (!EngagementDistanceReached())
            {
                state = EnemyState.Closing;

                var target = SubmarineController.Instance.transform;
                CalculateClosingOffset(target.position);
            }
        }
    }

    void MoveRotor()
    {
        rotor.Rotate(0f, 0f, rotorSpeed * Time.deltaTime);
    }

    void Rise()
    {
        if (transform.position.y >= _requiredHeight)
        {
            state = EnemyState.Closing;
            return;
        }

        transform.position += transform.up * (Time.deltaTime * riseSpeed);
    }

    void CheckHealth()
    {
        if (health <= 0 && state != EnemyState.Destroyed)
        {
            Die();
        }
    }

    void CloseDistance(Vector3 targetPosition)
    {
        if (EngagementDistanceReached())
        {
            _timeSinceLastShot = shootCoolDown;
            state = EnemyState.Attacking;
        }

        var movement = targetPosition - transform.position;

        Debug.DrawLine(transform.position, targetPosition);

        bool collides = Physics.Raycast(transform.position, transform.forward, collisionDistance, collisionLayerMask);
        Debug.DrawLine(transform.position, transform.position + transform.forward * collisionDistance, Color.blue);
        if (collides)
        {
            transform.position += transform.up * (Time.deltaTime * speed);
            return;
        }

        if (movement != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(movement, Vector3.up);
            transform.rotation =
                Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }

        transform.position += transform.forward * (Time.deltaTime * speed);
    }

    void FaceTarget()
    {
        var target = SubmarineController.Instance.transform;

        Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);

        // Interpolate the rotation smoothly over time
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    bool EngagementDistanceReached()
    {
        var target = SubmarineController.Instance.transform;

        return Vector3.Distance(transform.position, target.position) <= engagementDistance;
    }

    void Shoot()
    {
        Vector3 playerPosition = SubmarineController.Instance.transform.position;
        Vector3 playerVelocity = SubmarineController.Instance.GetComponent<Rigidbody>().velocity;

        // Calculate the time to hit the player
        float timeToHit = Vector3.Distance(transform.position, playerPosition) / bulletSpeed;

        // Calculate the future position
        Vector3 futurePosition = playerPosition + (playerVelocity * timeToHit);

        // Instantiate and Fire
        var projectileNO =
            NetworkObjectPool.Singleton.GetNetworkObject(rocket, rocketSpawn.position, rocketSpawn.rotation);
        projectileNO.Spawn();
        projectileNO.transform.localScale = Vector3.one * 2;
        HomingRocket homingRocket = projectileNO.GetComponent<HomingRocket>();
        homingRocket.target = futurePosition;
        homingRocket.activated = true;
        homingRocket.rocketPrefab = rocket;
    }

    // rocket auf enemy
    // enemy verhalten bei treffer
    // sam turret syncronisieren, station nimmt damage
}