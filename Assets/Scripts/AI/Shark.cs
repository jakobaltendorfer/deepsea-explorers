using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shark : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private float rotationSpeed = 100f;
    [SerializeField] private float speedChange = 1f;
    [SerializeField] private float speedMultiplier = 2f;
    [SerializeField] private float targetDistance = 20f;

    private GameObject[] _targets;
    private float _currMoveSpeed;
    private SharkState _state = SharkState.Hunting;
    private Vector3 _targetSwim;

    public enum SharkState
    {
        Hunting,
        Swimming,
    }

    // Start is called before the first frame update
    void Awake()
    {
        _targets = GameObject.FindGameObjectsWithTag("Target");
        _currMoveSpeed = speed;
    }

    GameObject GetClosestTarget()
    {
        float minDistance = float.MaxValue;
        GameObject target = _targets[0];
        foreach (var currTarget in _targets)
        {
            if (!currTarget.gameObject.activeSelf) continue;
            
            var distance = Vector3.Distance(currTarget.transform.position, transform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                target = currTarget;
            }
        }

        return target;
    }

    // Update is called once per frame
    void Update()
    {
        if (_targets.Length == 0) return;

        if (_state == SharkState.Hunting)
        {
            Hunt();
        }

        if (_state == SharkState.Swimming)
        {
            Swim();
        }
    }

    void Move(Vector3 target)
    {
        var movement = target - transform.position;

        if (movement != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(movement, Vector3.up);
            transform.rotation =
                Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }

        var targetMoveSpeed = speed;
        if (movement.magnitude < targetDistance)
            targetMoveSpeed *= speedMultiplier;

        _currMoveSpeed = Mathf.Lerp(_currMoveSpeed, targetMoveSpeed, Time.deltaTime * speedChange);

        transform.position += transform.forward * (Time.deltaTime * _currMoveSpeed);
    }

    void Hunt()
    {
        var target = GetClosestTarget();

        Move(target.transform.position);

        // shark swims to a new position
        if (Vector3.Distance(transform.position, target.transform.position) < 1.0f)
        {
            _targetSwim = transform.position + Random.insideUnitSphere.normalized * 200f;
            _state = SharkState.Swimming;
        }
    }

    void Swim()
    {
        Move(_targetSwim);

        // shark swims to a new position
        if (Vector3.Distance(transform.position, _targetSwim) < 1.0f)
        {
            _state = SharkState.Hunting;
        }

        if (Physics.Raycast(transform.position, transform.forward, 10f))
        {
            _state = SharkState.Hunting;
        }
    }
}