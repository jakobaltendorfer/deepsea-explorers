using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using Submarine;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.VFX;
using Random = UnityEngine.Random;

public class HomingRocket : NetworkBehaviour
{
    [SerializeField] private float speed = 10f;
    [SerializeField] private float rotationSpeed = 10f;
    [SerializeField] private float lifeTime = 10f;
    [SerializeField] private float triggerTime = 0.6f;
    [SerializeField] private GameObject explosion;
    [SerializeField] private int damage = 10;

    private NetworkVariable<bool> _isRendererEnabled = new NetworkVariable<bool>(true);

    private float elapsedLifeTime = 0.0f;
    public bool activated;
    public Vector3 target;
    private MeshRenderer _renderer;

    [HideInInspector] public GameObject rocketPrefab; // reference


    private void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
        _isRendererEnabled.OnValueChanged += HandleRendererValueChanged;
    }

    private void OnEnable()
    {
        transform.GetChild(0).gameObject.SetActive(true); // enable bubbles
        elapsedLifeTime = 0f;
        GetComponent<Collider>().enabled = true;

        if (IsHost)
            _isRendererEnabled.Value = true;
    }


    private void OnDisable()
    {
        GetComponent<Collider>().enabled = false;
        activated = false;
    }

    public override void OnNetworkSpawn()
    {
        if (!IsHost) return;

        GetComponent<Collider>().enabled = false;
        Invoke(nameof(EnableCollider), 0.8f);
    }

    private void HandleRendererValueChanged(bool oldValue, bool newValue)
    {
        _renderer.enabled = newValue;
    }

    private void EnableCollider()
    {
        GetComponent<Collider>().enabled = true;
    }


    void Update()
    {
        if (!IsHost) return;

        if (!activated)
        {
            return;
        }

        elapsedLifeTime += Time.deltaTime;

        if (elapsedLifeTime > lifeTime)
        {
            Kill();
        }

        if (Vector3.Distance(transform.position, target) <= 0.1f)
        {
            target = (transform.position + Random.onUnitSphere) * 1000;
        }

        Move();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!IsHost) return;

        if (elapsedLifeTime < triggerTime) return;

        var submarine = other.GetComponentInParent<SubmarineController>();
        if (submarine != null)
        {
            submarine.ChangeHealth(-damage);
        }

        var intensity = CameraShaker.Instance.IsInDistance(transform.position);
        if (intensity > 0f)
        {
            CameraShaker.Instance.ShakeCamera(4f * intensity, 0.5f);
        }

        Kill();
    }

    private void Kill()
    {
        var explosionGO = Instantiate(explosion, transform.position, Quaternion.identity);
        explosionGO.GetComponent<NetworkObject>().Spawn();

        GetComponent<AudioSource>().Play();
        GetComponent<Collider>().enabled = false;
        activated = false;
        _isRendererEnabled.Value = false;

        Destroy(explosionGO, 1f);
        Invoke(nameof(Despawn), 3f);
        transform.GetChild(0).gameObject.SetActive(false); // disable bubbles
    }

    private void Despawn()
    {
        var networkObject = GetComponent<NetworkObject>();
        networkObject.Despawn();
    }

    void Move()
    {
        var movement = target - transform.position;
        Debug.DrawLine(transform.position, target);

        if (movement != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(movement, Vector3.up);
            transform.rotation =
                Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }

        transform.position += transform.forward * (Time.deltaTime * speed);
    }
}