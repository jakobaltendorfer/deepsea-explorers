using System;
using System.Collections;
using System.Collections.Generic;
using Submarine;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Splines;
using Random = UnityEngine.Random;

public class Whale : NetworkBehaviour
{
    [SerializeField] private GameObject destroyedWhale;
    [SerializeField] private bool destroyable = true;
    [SerializeField] private float playbackSpeed = 1.0f;

    private int _bulletCount;

    private void Start()
    {
        GetComponent<Animator>().speed = playbackSpeed;
    }

    public override void OnNetworkSpawn()
    {
        Invoke(nameof(StartAnimation), 3f);
    }

    private void StartAnimation()
    {
        GetComponent<SplineAnimate>().Play();
    }


    private void StartDialog()
    {
        DialogueManager.Instance.StartDialogue("WhaleMishap");
    }

    [ServerRpc(RequireOwnership = false)]
    public void TakeDamageServerRpc()
    {
        if (!destroyable) return;

        _bulletCount++;
        if (_bulletCount == 3)
        {
            DialogueManager.Instance.RemoveMissionServerRpc("photographWhale");
            ExplodeClientRpc();
            Invoke(nameof(StartDialog), 4f);
        }
    }

    [ClientRpc]
    void ExplodeClientRpc()
    {
        var go = Instantiate(destroyedWhale, transform.position, transform.rotation);
        Destroy(go, 10f);

        Destroy(this.gameObject);
    }
}