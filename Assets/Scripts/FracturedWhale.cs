using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using Random = UnityEngine.Random;

public class FracturedWhale : NetworkBehaviour
{
    [SerializeField] private bool explode;
    [SerializeField] private float force = 1000f;

    private bool _exploded;

    private void Start()
    {
        Explode();
    }

    // Update is called once per frame
    void Update()
    {
        if (explode && !_exploded)
            Explode();
    }

    private void Explode()
    {
        _exploded = true;
        var pos = transform.position;

        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            var rb = child.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            float exForce = force * Random.Range(0.5f, 1.5f);
            rb.AddExplosionForce(force, pos, 25);
        }
    }
}