using UnityEngine;

namespace Cameras
{
    public class FollowMouseWorld : MonoBehaviour
    {
        // Start is called before the first frame update
        private Camera _mainCamera;

        void Start()
        {
            _mainCamera = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 mousePos = Input.mousePosition;
            var pos = transform.position;
            mousePos.z = Vector3.Distance(_mainCamera.transform.position, pos);

            var worldPosition = _mainCamera.ScreenToWorldPoint(mousePos);
            transform.position = new Vector3(Mathf.Clamp(worldPosition.x, -100, 100),
                Mathf.Clamp(worldPosition.y, -100, 100),
                pos.z);
        }
    }
}