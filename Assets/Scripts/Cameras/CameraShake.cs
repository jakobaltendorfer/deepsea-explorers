using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    
    public static CameraShake Instance { get; private set; }
    [SerializeField] private float Amplitude = 1f;
    [SerializeField] private float Frequency = 1f;
    [SerializeField] private Vector3 scale = Vector3.one;
    [SerializeField] private bool alwaysShake;

    private float _time = 0f;
    private Vector3 _startPos;
    // Start is called before the first frame update
    private void Awake()
    {
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        } 
    }
    

    void Start()
    {
        _startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;
        float x = (Mathf.PerlinNoise(_time * Frequency, 0) - 0.5f) * 2 * Amplitude;
        float y = (Mathf.PerlinNoise(0, _time * Frequency) - 0.5f) * 2 * Amplitude;
        float z = (Mathf.PerlinNoise(_time * Frequency, _time * Frequency) - 0.5f) * 2 * Amplitude;

        transform.position = new Vector3(_startPos.x + x * scale.x, _startPos.y + y * scale.y, _startPos.z + z * scale.z);
    }
}