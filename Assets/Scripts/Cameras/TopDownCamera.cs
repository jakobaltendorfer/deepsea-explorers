using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Submarine;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Cameras
{
    [RequireComponent(typeof(CinemachineFreeLook))]
    public class TopDownCamera : MonoBehaviour
    {
        [SerializeField] private float scrollSpeed = 0.5f;
        [SerializeField] private float lerpTime = 1f;
        [FormerlySerializedAs("xMoveSpeed")] [SerializeField] private float offsetMoveSpeed = 0.01f;

        private CinemachineFreeLook _cm;
        private float _targetYAxisValue = 0, _targetOffset;

        private Transform _localPlayer;
        private bool _hasLocalPlayer = false, _moving;
        private CinemachineCameraOffset _cameraOffset;
        private Coroutine _resetCameraOffsetCoroutine;

        private void Start()
        {
            GameInput.Instance.OnMouseScrollYAction += OnMouseScrollY;
            GameInput.Instance.OnRotatePressedAction += OnRotatePressed;
            GameInput.Instance.OnRotateReleasedAction += OnRotateReleased;


            _cm = GetComponent<CinemachineFreeLook>();
            _cm.m_XAxis.m_MaxSpeed = 0;
            _cm.m_YAxis.Value = 0.5f;
            _targetYAxisValue = _cm.m_YAxis.Value;

            _cameraOffset = GetComponent<CinemachineCameraOffset>();
            _cameraOffset.m_Offset = Vector3.zero;
        }

        private void OnMouseScrollY(object sender, GameInput.OnMouseScrollYActionEventArgs e)
        {
            if (CameraController.Instance.ActiveCamera != _cm) return;

            _targetYAxisValue = Mathf.Clamp01(_targetYAxisValue + e.value * scrollSpeed * -0.01f);
        }

        private void OnRotatePressed(object sender, EventArgs e)
        {
            if (CameraController.Instance.ActiveCamera != _cm) return;
            _moving = true;

            if (_resetCameraOffsetCoroutine != null)
            {
                StopCoroutine(_resetCameraOffsetCoroutine);
            }
        }

        private void OnRotateReleased(object sender, EventArgs e)
        {
            _moving = false;

            _resetCameraOffsetCoroutine = StartCoroutine(ResetCameraOffset());
        }

        IEnumerator ResetCameraOffset()
        {
            yield return new WaitForSeconds(1);
            _targetOffset = 0;
        }

        private void Update()
        {
            if (!_hasLocalPlayer && PlayerController.LocalPlayer != null)
            {
                _hasLocalPlayer = true;
                _localPlayer = PlayerController.LocalPlayer.transform;
                var target = _localPlayer.Find("Camera Target");
                _cm.m_Follow = target;
                _cm.m_LookAt = target;
            }

            _cm.m_YAxis.Value = Mathf.Lerp(_cm.m_YAxis.Value, _targetYAxisValue, Time.deltaTime / lerpTime);
            if (Mathf.Abs(_cm.m_YAxis.Value - _targetYAxisValue) < 0.001)
            {
                _cm.m_YAxis.Value = _targetYAxisValue;
            }

            if (_moving)
            {
                _targetOffset += Mouse.current.delta.value.x * offsetMoveSpeed;
            }

            float offsetX = Mathf.Lerp(_cameraOffset.m_Offset.x, _targetOffset, Time.deltaTime / lerpTime);
            _cameraOffset.m_Offset = new Vector3(offsetX, 0, 0);
        }

        private void LateUpdate()
        {
            var rotation = transform.rotation.eulerAngles;
            var submarineRot = SubmarineController.Instance.transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(rotation.x, submarineRot.y, rotation.z);
            _cm.m_Heading.m_Bias = submarineRot.y;
        }
    }
}