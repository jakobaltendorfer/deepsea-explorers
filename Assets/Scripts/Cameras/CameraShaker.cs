using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Submarine;
using Unity.Netcode;
using UnityEngine;

public class CameraShaker : NetworkBehaviour
{
    public static CameraShaker Instance { get; private set; }

    [SerializeField] private float shakeDistance = 100f;

    private float _shakerTime;

    private void Start()
    {
        CameraController.Instance.SetPerlinNoise(0);
    }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    // return between 0 and 1
    public float IsInDistance(Vector3 position)
    {
        float distance = Vector3.Distance(SubmarineController.Instance.transform.position, position);
        if (distance >= shakeDistance) return 0;
        return 1 - distance / shakeDistance;
    }

    public void ShakeCamera(float intensity, float time)
    {
        ShakeCameraServerRpc(intensity, time);
    }

    [ServerRpc(RequireOwnership = false)]
    private void ShakeCameraServerRpc(float intensity, float time)
    {
        ShakeCameraClientRpc(intensity, time);
    }

    [ClientRpc]
    private void ShakeCameraClientRpc(float intensity, float time)
    {
        Debug.Log("Shaking");

        CameraController.Instance.SetPerlinNoise(intensity);

        // var perlin = (CameraController.Instance.ActiveCamera as CinemachineVirtualCamera)
        //     .GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        // perlin.m_AmplitudeGain = intensity;
        _shakerTime = time;
    }


    private void Update()
    {
        if (_shakerTime > 0)
        {
            _shakerTime -= Time.deltaTime;
            if (_shakerTime <= 0f)
            {
                CameraController.Instance.SetPerlinNoise(0);
            }
        }
    }
}