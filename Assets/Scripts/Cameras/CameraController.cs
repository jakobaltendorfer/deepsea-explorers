using System;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance { get; private set; }

    [SerializeField] private CinemachineVirtualCameraBase topDownCamera;
    [SerializeField] private CinemachineVirtualCameraBase steeringCamera;
    [SerializeField] private CinemachineVirtualCameraBase leftGunCamera;
    [SerializeField] private CinemachineVirtualCameraBase rightGunCamera;


    private CinemachineVirtualCameraBase _activeCamera;
    private CameraType _activeCameraType;
    private float _shakerTime;

    public CinemachineVirtualCameraBase ActiveCamera => _activeCamera;
    public CameraType ActiveCameraType => _activeCameraType;

    public enum CameraType
    {
        TopDown,
        Steering,
        LeftGun,
        RightGun,
    }

    public void EnableGunCameraMovement()
    {
        GunCamera[] cams = GetComponentsInChildren<GunCamera>();
        foreach (var cam in cams)
        {
            var pov = cam.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachinePOV>();
            pov.m_VerticalAxis.m_MaxSpeed = cam.verticalSpeed;
            pov.m_HorizontalAxis.m_MaxSpeed = cam.horizontalSpeed;
        }
    }


    public void DisableGunCameraMovement()
    {
        GunCamera[] cams = GetComponentsInChildren<GunCamera>();
        foreach (var cam in cams)
        {
            var pov = cam.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachinePOV>();
            pov.m_VerticalAxis.m_MaxSpeed = 0;
            pov.m_HorizontalAxis.m_MaxSpeed = 0;
        }
    }

    private readonly List<CinemachineVirtualCameraBase> _cameras = new List<CinemachineVirtualCameraBase>();

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    private void Start()
    {
        _cameras.Add(topDownCamera);
        _cameras.Add(steeringCamera);
        _cameras.Add(leftGunCamera);
        _cameras.Add(rightGunCamera);

        _activeCamera = topDownCamera;
        _activeCameraType = CameraType.TopDown;

        Cursor.visible = false;
#if !UNITY_EDITOR
        Cursor.lockState = CursorLockMode.Confined; // Lock the cursor within the game window when not in Unity Editor.
#endif

        EnableGunCameraMovement();
    }


    // ReSharper disable Unity.PerformanceAnalysis
    public void SwitchCamera(CameraType newCameraType)
    {
        Debug.Log("Switching to camera " + newCameraType);
        _activeCameraType = newCameraType;
        switch (newCameraType)
        {
            case CameraType.TopDown:
                _activeCamera = topDownCamera;
                break;
            case CameraType.Steering:
                _activeCamera = steeringCamera;
                break;
            case CameraType.LeftGun:
                _activeCamera = leftGunCamera;
                break;
            case CameraType.RightGun:
                _activeCamera = rightGunCamera;
                break;
        }

        _activeCamera.Priority = 100;
        foreach (var cam in _cameras)
        {
            if (cam != _activeCamera)
            {
                cam.Priority = 10;
            }
        }
    }

    public void SetPerlinNoise(float intensity)
    {
        UpdatePerlin((topDownCamera as CinemachineFreeLook)!.GetRig(0), intensity);
        UpdatePerlin((topDownCamera as CinemachineFreeLook)!.GetRig(1), intensity);
        UpdatePerlin((topDownCamera as CinemachineFreeLook)!.GetRig(2), intensity);
        UpdatePerlin((steeringCamera as CinemachineFreeLook)!.GetRig(0), intensity);
        UpdatePerlin((steeringCamera as CinemachineFreeLook)!.GetRig(1), intensity);
        UpdatePerlin((steeringCamera as CinemachineFreeLook)!.GetRig(2), intensity);

        UpdatePerlin((leftGunCamera as CinemachineVirtualCamera), intensity);
        UpdatePerlin((rightGunCamera as CinemachineVirtualCamera), intensity);
    }

    private void UpdatePerlin(CinemachineVirtualCamera cam, float intensity)
    {
        if (cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>() != null)
            cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = intensity;
    }
}