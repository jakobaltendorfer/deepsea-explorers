using System;
using Cinemachine;
using UnityEngine;

namespace Cameras
{
    [RequireComponent(typeof(CinemachineFreeLook))]
    public class SteeringCamera : MonoBehaviour
    {
        [SerializeField] private float xAxisSpeed = 100f;
        [SerializeField] private float yAxisSpeed = 1f;
        [SerializeField] private float scrollSpeed = 0.5f;
        [SerializeField] private float maxRadius = 80f;
        [SerializeField] private float minRadius = 30f;
        [SerializeField] private float lerpTime = 0.5f;
        [SerializeField] private float radius = 50f;

        private CinemachineFreeLook _cm;
        private float _targetRadius;

        private void Start()
        {
            GameInput.Instance.OnMouseScrollYAction += OnMouseScrollY;
            GameInput.Instance.OnRotatePressedAction += OnRotatePressed;
            GameInput.Instance.OnRotateReleasedAction += OnRotateReleased;


            _cm = GetComponent<CinemachineFreeLook>();
            _cm.m_XAxis.m_MaxSpeed = 0;
            _cm.m_YAxis.m_MaxSpeed = 0;
            _targetRadius = radius;
            for (int i = 0; i < 3; i++)
            {
                _cm.m_Orbits[i].m_Radius = radius;
            }
        }

        private void OnMouseScrollY(object sender, GameInput.OnMouseScrollYActionEventArgs e)
        {
            if (CameraController.Instance.ActiveCamera != _cm) return;
            
            _targetRadius = Mathf.Clamp(_targetRadius + e.value * scrollSpeed * -0.1f, minRadius, maxRadius);
        }

        private void OnRotatePressed(object sender, EventArgs e)
        {
            if (CameraController.Instance.ActiveCamera != _cm) return;

            _cm.m_XAxis.m_MaxSpeed = xAxisSpeed;
            _cm.m_YAxis.m_MaxSpeed = yAxisSpeed;
            _cm.m_RecenterToTargetHeading.m_enabled = false;
            _cm.m_YAxisRecentering.m_enabled = false;
        }

        private void OnRotateReleased(object sender, EventArgs e)
        {
            _cm.m_XAxis.m_MaxSpeed = 0;
            _cm.m_YAxis.m_MaxSpeed = 0;
            _cm.m_RecenterToTargetHeading.m_enabled = true;
            _cm.m_YAxisRecentering.m_enabled = true;

        }

        private void Update()
        {
            for (int i = 0; i < 3; i++)
            {
                _cm.m_Orbits[i].m_Radius = Mathf.Lerp(_cm.m_Orbits[i].m_Radius, _targetRadius, Time.deltaTime / lerpTime);
            }
        }
    }
}