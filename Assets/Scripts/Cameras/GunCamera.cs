using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Submarine;
using UnityEngine;
using UnityEngine.ProBuilder.MeshOperations;

public class GunCamera : MonoBehaviour
{
    [SerializeField] private float offest = 0f;

    [SerializeField] private float xRange = 180f;
    [SerializeField] public float verticalSpeed = 0.05f;
    [SerializeField] public float horizontalSpeed = 0.1f;

    private CinemachineVirtualCamera _cm;
    private CinemachinePOV _pov;


    private bool _enabled = true;

    void Start()
    {
        _cm = GetComponent<CinemachineVirtualCamera>();
        _pov = _cm.GetCinemachineComponent<CinemachinePOV>();
    }

    public void Enable()
    {
        _enabled = true;
    }

    public void Disable()
    {
        _enabled = false;
    }

    void Update()
    {
        if (!_enabled) return;

        var sub = SubmarineController.Instance.transform;
        float yRotation = sub.eulerAngles.y + offest;
        _pov.m_HorizontalAxis.m_MaxValue = yRotation + xRange / 2;
        _pov.m_HorizontalAxis.m_MinValue = yRotation - xRange / 2;

        if (CameraController.Instance.ActiveCamera != _cm)
            _pov.m_HorizontalAxis.Value = yRotation;
    }
}