using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;

public class DollyCamera : MonoBehaviour
{
    public static DollyCamera Instance { get; private set; }
    [SerializeField] private bool skipIntro = false;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    public void PlayIntro()
    {
        if (skipIntro)
        {
            GetComponent<Animator>().enabled = false;
            GetComponent<CinemachineVirtualCamera>().m_Priority = 1;
        }

        Invoke(nameof(PlayerIntroDelayed), 2f);
    }

    private void PlayerIntroDelayed()
    {
        AudioManager.Instance.Play();

        Debug.Log("PlayerIntroDelayed");
        if (skipIntro)
        {
            Debug.Log("Skipping Intro");
            startIntroDialogue();
        }
        else
        {
            GetComponent<Animator>().SetTrigger("playIntro");
            Debug.Log("PlayerIntroDelayed Trigger");
        }
    }


    public void startIntroDialogue()
    {
        StartGame();
        // Invoke(nameof(StartGame), 2f);
    }

    private void StartGame()
    {
        GameManager.Instance.StartGame();
    }
}