using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class CoralReef : NetworkBehaviour
{
    [SerializeField] private GameObject smokeCenter;
    [SerializeField] private GameObject explosion;
    private int counter = 0;
    private int targetCount = 4;

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            counter++;
            if (counter == targetCount)
                StartCoralReefSequence();
        }
        else if (other.CompareTag("SpecialBullet"))
        {
            InstantiateExplosionServerRpc();
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void InstantiateExplosionServerRpc()
    {
        GameObject explosionGameObject =
            Instantiate(explosion, smokeCenter.transform.position, explosion.transform.rotation);
        explosionGameObject.GetComponent<NetworkObject>().Spawn();

        PlaySoundClientRpc();

        DialogueManager.Instance.RemoveMissionServerRpc("destroyCoralreef");
        Invoke(nameof(StartCoralReefDestroyedDialogue), 2.5f);
    }

    private void StartCoralReefDestroyedDialogue()
    {
        DialogueManager.Instance.StartDialogue("CoralreefDestroyed");
    }

    [ClientRpc]
    private void PlaySoundClientRpc()
    {
        GetComponent<AudioSource>().Play();
    }

    private void StartCoralReefSequence()
    {
        DialogueManager.Instance.StartDialogue("CoralreefExplosion");
    }
}