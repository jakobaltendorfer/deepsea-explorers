using Submarine;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.VFX;

public class SubmarineMovement : NetworkBehaviour
{
    public static SubmarineMovement Instance { get; private set; }

    [SerializeField] private float yawSpeed = 50f;
    [SerializeField] private float acceleration = 800f;
    [SerializeField] private Transform propeller;
    [SerializeField] private float propellerSpeed = 60f;
    [SerializeField] private float ascendSpeed = 20f;
    [SerializeField] private float ascendForce = 800f;
    [SerializeField] private float ascendRotation = 10f;
    [SerializeField] private float ascendRotationSpeed = 100f;
    [SerializeField] private float returnRotationSpeed = 15f;

    private Rigidbody _rb;
    private Transform _submarine;
    private float _ascend;
    private Vector2 _movement;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }


    private void Start()
    {
        _submarine = SubmarineController.Instance.transform;
        _rb = _submarine.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        Invoke(nameof(StartDialog), 1.1f);

        Invoke(nameof(DelayedOwner), 0.5f);
        transform.GetChild(0).gameObject.SetActive(true);
        propeller.GetComponent<VisualEffect>().Play();
    }


    private void StartDialog()
    {
        DialogueManager.Instance.StartDialogue("Coralreef");
    }

    private void DelayedOwner()
    {
        var owner = transform.parent.GetComponent<EnterModule>().PlayerId;
        Debug.Log("Owner " + owner);

        ChangeOwnerShipServerRpc(owner);
    }

    [ServerRpc(RequireOwnership = false)]
    private void ChangeOwnerShipServerRpc(ulong owner)
    {
        SubmarineController.Instance.GetComponent<NetworkObject>().ChangeOwnership(owner);
    }


    private void OnDisable()
    {
        propeller.GetComponent<VisualEffect>().Stop();

        if (IsHost)
        {
            SubmarineController.Instance.GetComponent<NetworkObject>().RemoveOwnership();
        }

        transform.GetChild(0).gameObject.SetActive(false);
    }

    void Update()
    {
        HandleAscend();
        HandleMovement();
        ReturnToNeutralRotation();

        RotatePropellerServerRpc(_rb.velocity.magnitude);
    }

    [ServerRpc(RequireOwnership = false)]
    void RotatePropellerServerRpc(float speed)
    {
        RotatePropellerClientRpc(speed);
    }

    [ClientRpc]
    void RotatePropellerClientRpc(float speed)
    {
        speed = Mathf.Clamp(speed, 3f, 25f);
        if (propeller)
            propeller.Rotate(Vector3.right, speed * propellerSpeed * Time.deltaTime);
    }

    void HandleAscend()
    {
        if (SubmarineController.Instance.Health <= 0)
            _ascend = 0f;
        else
            _ascend = GameInput.Instance.GetAscendAxis();

        float up = _rb.velocity.y;
        if (Mathf.Abs(up) > ascendSpeed)
        {
            up = Mathf.Clamp(up, -ascendSpeed, ascendSpeed);
            _rb.velocity = new Vector3(_rb.velocity.x, up, _rb.velocity.z);
        }

        // Ascend rotation
        float targetRotationZ = up / ascendSpeed * ascendRotation;
        var rotation = _submarine.rotation;
        var targetRotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y, targetRotationZ);
        _submarine.rotation = Quaternion.RotateTowards(rotation, targetRotation, ascendRotationSpeed * Time.deltaTime);
    }

    void HandleMovement()
    {
        if (SubmarineController.Instance.Health <= 0)
            _movement = Vector2.zero;
        else
            _movement = GameInput.Instance.GetMovementVector();
    }

    void ReturnToNeutralRotation()
    {
        // Check if X rotation is not approximately 0.
        if (!Mathf.Approximately(_submarine.eulerAngles.x, 0f))
        {
            // Calculate the target rotation.
            Vector3 targetRotation = new Vector3(0, _submarine.eulerAngles.y, _submarine.eulerAngles.z);

            // Rotate back to 0 on the X-axis.
            _submarine.rotation = Quaternion.RotateTowards(_submarine.rotation, Quaternion.Euler(targetRotation),
                returnRotationSpeed * Time.deltaTime);
        }
    }


    private void FixedUpdate()
    {
        var aforce = Vector3.up * (_ascend * ascendForce * Time.fixedDeltaTime);
        _rb.AddForce(aforce, ForceMode.Acceleration);

        var movementForce = _submarine.right * (_movement.y * acceleration * Time.fixedDeltaTime);
        _rb.AddForce(movementForce, ForceMode.Acceleration);

        var torque = _movement.x * yawSpeed * Time.fixedDeltaTime;
        _rb.AddTorque(0, torque, 0, ForceMode.Acceleration);
    }
}