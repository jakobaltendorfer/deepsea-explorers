using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightOnLook : MonoBehaviour
{
    [ColorUsage(false, true)] [SerializeField]
    private Color emissionColor;

    [SerializeField] private MonoBehaviour interactable;

    private bool _isHighlighted;
    private MeshRenderer[] _childRenderers = { };
    private PlayerController _player;

    private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");

    void Start()
    {
        if (!(interactable is IInteractable))
        {
            Debug.LogError("Assigned reference doesn't implement IInteractable! " + gameObject.name);
        }

        _childRenderers = GetComponentsInChildren<MeshRenderer>();
        GameInput.Instance.OnInteractAction += OnInteractPerformed;
    }

    private void OnInteractPerformed(object sender, EventArgs e)
    {
        if (_player == null)
            return;

        (interactable as IInteractable)?.Interact(_player);
        _player.IsInteracting();
    }


    public void OnLookAt(PlayerController player)
    {
        if (_isHighlighted) return;

        _player = player;
        _isHighlighted = true;
        for (int i = 0; i < _childRenderers.Length; i++)
        {
            _childRenderers[i].material.EnableKeyword("_EMISSION");
            _childRenderers[i].material.SetColor(EmissionColor, emissionColor);
        }
    }


    public void OnLookAway()
    {
        if (!_isHighlighted) return;
        _isHighlighted = false;
        _player = null;
        for (int i = 0; i < _childRenderers.Length; i++)
        {
            _childRenderers[i].material.DisableKeyword("_EMISSION");
        }
    }
}