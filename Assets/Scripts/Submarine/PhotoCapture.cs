using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Submarine.Modules
{
    public class PhotoCapture : MonoBehaviour
    {
        [SerializeField] private float maxDistance = 300f;
        [SerializeField] private AudioSource cameraAudio;

        private List<PhotoObject> photoObjects = new List<PhotoObject>();
        private Texture2D screenCapture;
        private bool viewPhoto;

        void Start()
        {
            GameInput.Instance.OnExitAction += OnExitPerformed;
            screenCapture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        }

        private void OnExitPerformed(object sender, EventArgs e)
        {
            enabled = false;
        }

        private void OnEnable()
        {
            GameInput.Instance.OnShootAction += ShootPerformed;
            GameInput.Instance.OnPhotoAction += PhotoPerformed;
            PlayerCanvas.Instance.EnableCameraBackground(true);
            var objects = GameObject.FindObjectsByType<PhotoObject>(FindObjectsSortMode.None);
            photoObjects.Clear();
            foreach (var photoGameObject in objects)
            {
                photoObjects.Add(photoGameObject.GetComponent<PhotoObject>());
            }
        }

        private void OnDisable()
        {
            GameInput.Instance.OnShootAction -= ShootPerformed;
            GameInput.Instance.OnPhotoAction -= PhotoPerformed;
            PlayerCanvas.Instance.EnableCameraBackground(false);
            PlayerCanvas.Instance.EnablePhotoFrame(false);
        }

        private void ShootPerformed(object sender, EventArgs args)
        {
            if (!viewPhoto)
                StartCoroutine(TakePhoto());
            else
                RemovePhoto();
        }

        IEnumerator TakePhoto()
        {
            cameraAudio.Play();
            viewPhoto = true;
            yield return new WaitForEndOfFrame();
            CheckForObjects();
            Rect regionToRead = new Rect(0, 0, Screen.width, Screen.height);
            screenCapture.ReadPixels(regionToRead, 0, 0, false);
            screenCapture.Apply();

            ShowPhoto();
        }

        private void CheckForObjects()
        {
            foreach (var photoObject in photoObjects)
            {
                float distance = Vector3.Distance(transform.position, photoObject.transform.position);
                if (distance < maxDistance)
                {
                    Vector3 viewportPoint = Camera.main.WorldToViewportPoint(photoObject.transform.position);
                    if (viewportPoint.x is > 0.1f and < 0.9f && viewportPoint.y is > 0 and < 1 &&
                        viewportPoint.z > 0)
                    {
                        photoObject.onPhoto.Invoke();
                    }
                }
            }
        }

        void ShowPhoto()
        {
            Sprite photoSprite = Sprite.Create(screenCapture,
                new Rect(0.0f, 0.0f, screenCapture.width, screenCapture.height), new Vector2(0.5f, 0.5f), 100.0f);

            PlayerCanvas.Instance.SetPhotoFrameSprite(photoSprite);
            SaveSprite(photoSprite);

            PlayerCanvas.Instance.EnablePhotoFrame(true);
            PlayerCanvas.Instance.EnableCameraBackground(false);
        }

        private void PhotoPerformed(object sender, EventArgs e)
        {
            GetComponent<DoubleGuns>().enabled = true;
            enabled = false;
        }


        private void RemovePhoto()
        {
            PlayerCanvas.Instance.EnableCameraBackground(true);
            Debug.Log("Remove photo");
            viewPhoto = false;
            PlayerCanvas.Instance.EnablePhotoFrame(false);
        }

        public void SaveSprite(Sprite sprite)
        {
            string directory;

            // Check if running in Unity Editor or standalone build
            if (Application.isEditor)
            {
                directory = Path.Combine(Application.dataPath, "Screenshots");
                // Create the directory if it doesn't exist
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
            else
            {
                directory = Directory.GetParent(Application.dataPath)!.FullName; // One directory up from *_Data folder
            }

            // Generate the timestamp-based filename using underscores instead of colons
            string filename = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";

            // Convert the sprite to a Texture2D
            Texture2D texture = SpriteToTexture2D(sprite);

            // Convert the Texture2D to a byte array (PNG format)
            byte[] bytes = texture.EncodeToPNG();

            // Write the byte array to a file
            File.WriteAllBytes(Path.Combine(directory, filename), bytes);
        }

        private static Texture2D SpriteToTexture2D(Sprite sprite)
        {
            if (Math.Abs(sprite.rect.width - sprite.texture.width) > 0.01f)
            {
                Texture2D newText = new Texture2D((int) sprite.rect.width, (int) sprite.rect.height);
                Color[] newColors = sprite.texture.GetPixels((int) sprite.textureRect.x,
                    (int) sprite.textureRect.y,
                    (int) sprite.textureRect.width,
                    (int) sprite.textureRect.height);
                newText.SetPixels(newColors);
                newText.Apply();
                return newText;
            }

            return sprite.texture;
        }
    }
}