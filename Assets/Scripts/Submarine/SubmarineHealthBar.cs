using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubmarineHealthBar : MonoBehaviour
{
    public static SubmarineHealthBar Instance { get; private set; }

    [SerializeField] private Color healthColor = Color.green;
    [SerializeField] private Color criticalColor = Color.red;
    [SerializeField] private Transform fill;
    [SerializeField] private float repairCooldown = 1f;

    private Slider _slider;
    private Camera _camera;
    private float _targetValue;
    private float _elapsedTime = 0f; // Time passed since the start of the Lerp
    private float _initialValue; // Starting value for the Lerp

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        _slider = GetComponent<Slider>();
        _camera = Camera.main;
    }

    public void UpdateHealthBar(int currentValue, int maxValue, bool critical, bool animate)
    {
        PlayerCanvas.Instance.UpdateHealthBar(currentValue, maxValue, critical);

        Debug.Log("Update Health Bar");
        _targetValue = currentValue / (float) maxValue;
        if (!animate)
        {
            _initialValue = _targetValue;
            GetComponent<Slider>().value = _targetValue;
            _elapsedTime = 0f; // Reset the elapsed time if not animating
        }
        else
        {
            _initialValue = GetComponent<Slider>().value; // Set the starting value when beginning the Lerp
        }

        fill.GetComponent<Image>().color = critical ? criticalColor : healthColor;
    }

    private void Update()
    {
        transform.LookAt(transform.position + _camera.transform.rotation * Vector3.forward,
            _camera.transform.rotation * Vector3.up);

        if (Mathf.Abs(_slider.value - _targetValue) > 0.01f)
        {
            _elapsedTime += Time.deltaTime;
            float lerpFraction = _elapsedTime / repairCooldown;
            _slider.value =
                Mathf.Lerp(_initialValue, _targetValue, lerpFraction); // Lerp between the initial and target values
        }
        else
        {
            _elapsedTime = 0f;
        }
    }
}