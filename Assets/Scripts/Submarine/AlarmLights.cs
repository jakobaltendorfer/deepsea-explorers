using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmLights : MonoBehaviour
{
    [SerializeField] private float lightIntensity = 3f;
    [SerializeField] private float lerpSpeed = 1f;

    private readonly List<Light> _lights = new List<Light>();

    private float _targetIntensity = 0f;
    private float _currIntensity = 0f;
    private AudioSource _audio;

    [SerializeField] private bool _alarm;

    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var light = transform.GetChild(i).GetComponent<Light>();
            _lights.Add(light);
            light.intensity = 0;
            light.enabled = false;
        }

        _audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_alarm)
        {
            if (Mathf.Abs(_currIntensity - _targetIntensity) < 0.01)
            {
                _targetIntensity = _targetIntensity == 0f ? lightIntensity : 0f;
            }
        }
        else
        {
            _targetIntensity = 0f;
        }

        if (Mathf.Abs(_currIntensity - _targetIntensity) < 0.005) return;
        
        foreach (var light in _lights)
        {
            light.intensity = Mathf.Lerp(light.intensity, _targetIntensity, Time.deltaTime * lerpSpeed);
            _currIntensity = light.intensity;
            if (_currIntensity < 0.05f)
            {
                light.enabled = false;
            }
            else
            {
                light.enabled = true;
            }
        }
    }

    public void SetAlarm(bool active)
    {
        if (active == _alarm) return;
        
        _alarm = active;
        if (active)
        {
            _audio.Play();
        }
        else
        {
            _audio.Stop();
        }
    }
}