using Networking;
using UnityEngine;

namespace Submarine
{
    public class SubmarineVisual : MonoBehaviour
    {
        public static SubmarineVisual Instance { get; private set; }

        [SerializeField] private Transform submarineTop;
        [SerializeField] private Transform submarineSteering;
        [SerializeField] private LightSwitcher lightSwitcher;

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(this);
            else
                Instance = this;
        }

        void Start()
        {
            submarineTop.gameObject.SetActive(false);
            submarineSteering.gameObject.SetActive(true);
        }


        public void ActivateSteering()
        {
            submarineTop.gameObject.SetActive(false);
            submarineSteering.gameObject.SetActive(true);
        }

        public void ActivateTopDown()
        {
            submarineTop.gameObject.SetActive(true);
            submarineSteering.gameObject.SetActive(false);
        }

        public void SwitchLights(bool active)
        {
            Debug.Log("SwitchLights");
            lightSwitcher.StartSwitchLights(active);
            StartEngine(active);
        }

        public void StartEngine(bool active)
        {
            transform.parent.GetComponentInChildren<Repair>().StartEngine(active);
        }
    }
}