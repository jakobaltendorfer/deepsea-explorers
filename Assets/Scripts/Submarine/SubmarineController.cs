using System;
using Unity.Netcode;
using UnityEngine;

namespace Submarine
{
    public class SubmarineController : NetworkBehaviour
    {
        public static SubmarineController Instance { get; private set; }

        [SerializeField] private int maxHealth = 100;
        [SerializeField] private int criticalHealth = 20;

        [SerializeField] private NetworkVariable<int> health = new NetworkVariable<int>();

        private AlarmLights _alarm;
        private bool _hasNotifiedRepair;

        public int Health => health.Value;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            _alarm = FindAnyObjectByType<AlarmLights>();
            SubmarineHealthBar.Instance.UpdateHealthBar(health.Value, maxHealth, false, false);
            Debug.Log("SUBMARINE START");
        }

        public override void OnNetworkSpawn()
        {
            if (IsHost)
                health.Value = maxHealth;
            SubmarineHealthBar.Instance.UpdateHealthBar(health.Value, maxHealth, false, false);
        }

        public void ChangeHealth(int value)
        {
            UpdateHealthServerRpc(value);
        }

        [ServerRpc(RequireOwnership = false)]
        private void UpdateHealthServerRpc(int value)
        {
            if (value < 0 && !_hasNotifiedRepair)
            {
                _hasNotifiedRepair = true;
                DialogueManager.Instance.StartDialogue("Repair");
            }

            health.Value = Math.Clamp(health.Value + value, 0, maxHealth);

            if (health.Value <= 0)
            {
                DialogueManager.Instance.StartDialogue("EngineFailure");
            }

            CheckCriticalClientRpc(health.Value);
        }

        [ClientRpc]
        private void CheckCriticalClientRpc(int currentHealth)
        {
            bool isCritical = currentHealth <= criticalHealth;
            _alarm.SetAlarm(isCritical);
            SubmarineHealthBar.Instance.UpdateHealthBar(currentHealth, maxHealth, isCritical, true);
            SubmarineVisual.Instance.SwitchLights(currentHealth > 0);
        }

        public bool CanRepair()
        {
            return health.Value < maxHealth;
        }
    }
}