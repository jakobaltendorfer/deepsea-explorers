using System;
using System.Collections;
using Submarine;
using Unity.Netcode;
using UnityEngine;

public class EnterModule : NetworkBehaviour, IInteractable
{
    [SerializeField] private CameraController.CameraType activeCamera; // switches to that camera on enter module
    [SerializeField] private GameObject module;
    [SerializeField] private Transform positionTarget;
    [SerializeField] private Transform hideTarget;
    [SerializeField] private Transform exitPosition;

    private readonly NetworkVariable<bool> _isActive = new NetworkVariable<bool>(false);

    private readonly NetworkVariable<ulong> _playerId = new NetworkVariable<ulong>();

    public ulong PlayerId => _playerId.Value;


    private void Awake()
    {
        module.SetActive(false);
    }

    void Start()
    {
        GameInput.Instance.OnInteractAction += OnExitPerformed;
    }

    private void OnExitPerformed(object sender, EventArgs e)
    {
        StartCoroutine(Exit());
    }


    [ServerRpc(RequireOwnership = false)]
    private void EnterModuleServerRpc(bool value)
    {
        _isActive.Value = value;
    }

    private void EnableModule(bool active)
    {
        var currentPlayer = PlayerController.LocalPlayer;
        Debug.Log("EnableModule " + active);
        currentPlayer.enabled = !active;
        currentPlayer.GetComponent<PlayerMovement>().enabled = !active;

        if (hideTarget != null)
            hideTarget.gameObject.SetActive(!active);

        if (active)
        {
            currentPlayer.transform.position = positionTarget.position;
            foreach (var renderer in currentPlayer.GetComponentsInChildren<Renderer>())
                renderer.enabled = false;
            Invoke(nameof(ActivateSteering), 1f);
        }
        else
        {
            currentPlayer.transform.position = exitPosition.position;
            foreach (var renderer in currentPlayer.GetComponentsInChildren<Renderer>())
                renderer.enabled = true;
            Invoke(nameof(ActivateTopDown), 1f);
        }

        CameraController.Instance.SwitchCamera(active ? activeCamera : CameraController.CameraType.TopDown);
        module.SetActive(active);

        var playerId = currentPlayer.GetComponent<NetworkObject>().OwnerClientId;
        UpdateOwnershipServerRpc(active, playerId);
    }

    private void ActivateSteering()
    {
        SubmarineVisual.Instance.ActivateSteering();
    }
    
    private void ActivateTopDown()
    {
        SubmarineVisual.Instance.ActivateTopDown();
    }

    [ServerRpc(RequireOwnership = false)]
    private void UpdateOwnershipServerRpc(bool active, ulong playerId)
    {
        var networkObjects = GetComponentsInChildren<NetworkObject>();
        foreach (var networkObject in networkObjects)
        {
            if (active)
            {
                if (networkObject.IsSpawned && networkObject.OwnerClientId != playerId)
                    networkObject.ChangeOwnership(playerId);
            }
            else
            {
                networkObject.RemoveOwnership();
            }
        }
    }

    public void Interact(PlayerController playerController)
    {
        StartCoroutine(DelayedInteract(playerController));
    }

    IEnumerator DelayedInteract(PlayerController playerController)
    {
        if (!_isActive.Value)
        {
            yield return new WaitForSeconds(0.2f);
            if (!_isActive.Value)
            {
                Debug.Log("DelayedInteract " + gameObject.name);

                AssignPlayerServerRpc(playerController.GetComponent<NetworkObject>().OwnerClientId);
                playerController.CurrentModule = this;

                EnterModuleServerRpc(true);
                EnableModule(true);
            }
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void AssignPlayerServerRpc(ulong playerId)
    {
        _playerId.Value = playerId;
    }

    IEnumerator Exit()
    {
        if (_isActive.Value)
        {
            yield return new WaitForSeconds(0.2f);
            if (_isActive.Value)
            {
                Debug.Log("Exit " + gameObject.name);
                Debug.Log("Player " + _playerId.Value);
                Debug.Log("LocalPlayer " + PlayerController.LocalPlayer.OwnerClientId);

                if (_playerId.Value == PlayerController.LocalPlayer.OwnerClientId)
                {
                    EnterModuleServerRpc(false);
                    EnableModule(false);
                }
            }
        }
    }
}

public interface IInteractable
{
    public void Interact(PlayerController player)
    {
    }
}