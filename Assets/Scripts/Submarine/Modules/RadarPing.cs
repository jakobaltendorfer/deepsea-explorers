﻿/* 
    ------------------- Code Monkey -------------------
    
    Thank you for downloading the Code Monkey Utilities
    I hope you find them useful in your projects
    If you have any questions use the contact form
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System;
using UnityEngine;

namespace Submarine.Modules
{
    public class RadarPing : MonoBehaviour
    {
        private SpriteRenderer _spriteRenderer;
        private float _disappearTimer;
        private float _disappearTimerMax;
        private Color _color;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _disappearTimerMax = 1f;
            _disappearTimer = 0f;
            _color = new Color(1, 1, 1, 1f);
        }


        private void Update()
        {
            _disappearTimer += Time.deltaTime;

            _color.a = Mathf.Lerp(_disappearTimerMax, 0f, _disappearTimer / _disappearTimerMax);
            _spriteRenderer.color = _color;

            if (_disappearTimer >= _disappearTimerMax)
            {
                Destroy(gameObject);
            }
        }

        public void SetColor(Color color)
        {
            _color = color;
        }

        public void SetDisappearTimer(float disappearTimerMax)
        {
            _disappearTimerMax = disappearTimerMax;
            _disappearTimer = 0f;
        }
    }
}