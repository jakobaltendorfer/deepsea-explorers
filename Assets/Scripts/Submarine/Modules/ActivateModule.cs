using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateModule : MonoBehaviour
{
    [SerializeField] private MonoBehaviour script;


    private void OnEnable()
    {
        script.enabled = true;
    }

    private void OnDisable()
    {
        script.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
    }
}