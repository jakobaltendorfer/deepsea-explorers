﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Serialization;

namespace Submarine.Modules
{
    public class SonarPulse : MonoBehaviour
    {
        [SerializeField] private Transform pfRadarPing;
        [SerializeField] private LayerMask radarLayerMask;
        [SerializeField] private float rangeMax = 300f;
        [SerializeField] private float fadeRange = 50f;
        [SerializeField] private float rangeSpeed = 100f;
        [SerializeField] private float pingTime = 1f;
        [SerializeField] private Color pingColor = new Color(1, 0, 0);
        [SerializeField] private Color questPingColor = new Color(1, 1, 0);

        private Transform _pulseTransform;
        private float _range;
        private SpriteRenderer _pulseSpriteRenderer;
        private Color _pulseColor;
        private int _questLayer;
        private readonly List<Transform> _alreadyPinged = new List<Transform>();
        private AudioSource _sonarAudio;
        private GameObject[] _quests;

        private void Awake()
        {
            _pulseTransform = transform.Find("Pulse");
            _pulseSpriteRenderer = _pulseTransform.GetComponent<SpriteRenderer>();
            _sonarAudio = GetComponent<AudioSource>();
            _pulseColor = _pulseSpriteRenderer.color;
            _questLayer = LayerMask.NameToLayer("Quest");
            GetComponentInChildren<Camera>().orthographicSize = rangeMax / 2f;
        }

        private void Start()
        {
            _quests = GameObject.FindGameObjectsWithTag("Quest");
        }


        private void OnEnable()
        {
            PlayerCanvas.Instance.EnableSonar(true);
            _sonarAudio.Play();
        }

        private void OnDisable()
        {
            PlayerCanvas.Instance.EnableSonar(false);
        }

        private void Update()
        {
            _range += rangeSpeed * Time.deltaTime;
            if (_range > rangeMax)
            {
                _range = 0f;
                _alreadyPinged.Clear();
                _sonarAudio.Play();
                _quests = GameObject.FindGameObjectsWithTag("Quest");
                AddQuestPings();
            }

            _pulseTransform.localScale = new Vector3(_range, _range);
            Collider[] colliders = Physics.OverlapSphere(transform.position, _range / 1.5f, radarLayerMask);

            foreach (var item in colliders)
            {
                // skip destroyed enemies
                if (item.GetComponent<EnemySubmarine>().state == EnemySubmarine.EnemyState.Destroyed)
                {
                    //print("Pinging Destroyed enemy");
                    continue;
                }

                if (!_alreadyPinged.Contains(item.transform))
                {
                    _alreadyPinged.Add(item.transform);
                    //CMDebug.TextPopup("Ping!", raycastHit2D.point);
                    Transform radarPingTransform =
                        //Instantiate(pfRadarPing, item.transform.position, Quaternion.Euler(90, 0, 0));
                        Instantiate(pfRadarPing, item.transform.position, Quaternion.Euler(90, 0, 0));
                    RadarPing radarPing = radarPingTransform.GetComponent<RadarPing>();
                    radarPing.SetColor(pingColor);

                    radarPing.SetDisappearTimer(pingTime);
                }
            }

            // Fade Pulse
            if (_range > rangeMax - fadeRange)
            {
                _pulseColor.a = Mathf.Lerp(0f, 1f, (rangeMax - _range) / fadeRange);
            }
            else
            {
                _pulseColor.a = 1f;
            }

            _pulseSpriteRenderer.color = _pulseColor;
        }

        private void AddQuestPings()
        {
            foreach (var quest in _quests)
            {
                if (!quest.gameObject.activeSelf) continue;

                if (!_alreadyPinged.Contains(quest.transform))
                {
                    _alreadyPinged.Add(quest.transform);

                    var questPosition = quest.transform.position;
                    float distance = Vector3.Distance(questPosition, transform.position);
                    var sonarPos = transform.position;
                    if (distance > rangeMax * 0.5f * 0.95f)
                    {
                        questPosition = sonarPos + (questPosition - sonarPos).normalized * (rangeMax * 0.5f * 0.95f);
                    }

                    Transform radarPingTransform =
                        Instantiate(pfRadarPing, questPosition, Quaternion.Euler(90, 0, 0));
                    RadarPing radarPing = radarPingTransform.GetComponent<RadarPing>();
                    radarPing.SetColor(questPingColor);

                    radarPing.SetDisappearTimer(pingTime);
                }
            }
        }
    }
}