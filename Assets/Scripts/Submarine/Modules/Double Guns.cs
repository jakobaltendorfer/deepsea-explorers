using System;
using System.Collections;
using System.Collections.Generic;
using Networking;
using Submarine;
using TMPro;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Windows.WebCam;
using PhotoCapture = Submarine.Modules.PhotoCapture;
using Random = UnityEngine.Random;

public class DoubleGuns : NetworkBehaviour
{
    [SerializeField] private Transform tower;
    [SerializeField] private Transform barrel1;
    [SerializeField] private Transform barrel2;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject specialBullet;
    [SerializeField] private Vector3 bulletRotationOffset;
    [SerializeField] private int maxBulletCount = 32;
    [SerializeField] private Vector3 towerRotationOffset;
    [SerializeField] private float bulletTranslationOffset = 1.6f;
    [SerializeField] private float delay = 1f;
    [SerializeField] private float shootCooldown = 0.1f;
    [SerializeField] private float shootScale = 0.8f;
    [SerializeField] private float randomRotation = 0.5f;
    [SerializeField] private float lerpSpeed = 20f;
    [SerializeField] private AudioSource audio1;
    [SerializeField] private AudioSource audio2;

    private Camera _camera;
    private Quaternion _towerOffset, _initialRotationBarrel;
    private bool _isLeftGun;
    private float _timer, _shootTimer;
    private int bulletCount;
    private bool hasSpecialBullet, _canShoot = true;

    private void Start()
    {
        _camera = Camera.main;
        _towerOffset = tower.localRotation;

        bulletCount = maxBulletCount;
        UpdateBulletUI(bulletCount);

        GameInput.Instance.OnShootAction += OnShootAction;

        DialogueManager.Instance.StartDialogue("Reload");
    }

    private void OnShootAction(object sender, EventArgs e)
    {
        _canShoot = true;
    }

    private void OnEnable()
    {
        _canShoot = true;
        GameInput.Instance.OnPhotoAction += OnPhotoPerformed;

        PlayerCanvas.Instance.EnableCrosshair(true);
        if (PlayerController.LocalPlayer.pickedAmmo)
        {
            PlayerController.LocalPlayer.pickedAmmo = false;
            PlayerCanvas.Instance.EnablePickUpAmmo(AmmoBox.AmmoType.Default, false);
            UpdateBulletUI(maxBulletCount);
        }
        else
            UpdateBulletUI(bulletCount);

        if (PlayerController.LocalPlayer.pickedSpecialAmmo)
        {
            hasSpecialBullet = true;
            PlayerCanvas.Instance.EnablePickUpAmmo(AmmoBox.AmmoType.Special, false);
            PlayerController.LocalPlayer.pickedSpecialAmmo = false;
            DialogueManager.Instance.RemoveMissionServerRpc("coralreefExplosion");
            UpdateBulletUI(bulletCount);
        }

        _timer = delay;
        _initialRotationBarrel = barrel1.rotation;
    }

    private void OnDisable()
    {
        if (!enabled) // if enables == true the object gets destroyed
            PlayerCanvas.Instance.EnableCrosshair(false);

        GameInput.Instance.OnPhotoAction -= OnPhotoPerformed;
        // tower.rotation = _towerOffset;
        // barrel1.rotation = _initialRotationBarrel;
        // barrel2.rotation = _initialRotationBarrel;
    }

    private void UpdateBulletUI(int count)
    {
        PlayerCanvas.Instance.UpdateBulletCount(count, maxBulletCount, hasSpecialBullet);
        bulletCount = count;
    }

    void Update()
    {
        // delay because of camera animation
        if (_timer > 0f)
        {
            _timer -= Time.deltaTime;
            return;
        }

        HandleRotation();
        HandleShoot();
        HandleBarrelScale();
    }

    private void OnPhotoPerformed(object sender, EventArgs e)
    {
        GetComponent<PhotoCapture>().enabled = true;
        enabled = false;
    }

    void HandleBarrelScale()
    {
        var otherBarrel = _isLeftGun ? barrel1 : barrel2;
        var activeBarrel = _isLeftGun ? barrel2 : barrel1;

        float scale = Mathf.Lerp(1, shootScale, _shootTimer / shootCooldown);
        activeBarrel.localScale = new Vector3(1, 1, scale);
        otherBarrel.localScale = Vector3.one;
    }

    void HandleShoot()
    {
        if (_shootTimer > 0)
        {
            _shootTimer -= Time.deltaTime;
            return;
        }

        if (GameInput.Instance.IsShootPressed() && hasSpecialBullet)
        {
            _canShoot = false; // Player has to press shoot again
            Shoot();
            hasSpecialBullet = false;
            UpdateBulletUI(bulletCount);
        }
        else if (GameInput.Instance.IsShootPressed() && bulletCount > 0 && _canShoot)
        {
            Shoot();
            bulletCount--;
            UpdateBulletUI(bulletCount);
            _shootTimer = shootCooldown;
        }
    }


    void Shoot()
    {
        Debug.Log("Shoot");
        Vector3 screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 30);
        var aim = _camera.ScreenToWorldPoint(screenCenter);

        ShootServerRpc(aim, hasSpecialBullet);

        if (_isLeftGun)
            audio1.Play();
        else
            audio2.Play();
    }

    Quaternion RemoveXAndZAxis(Quaternion input)
    {
        Vector3 euler = input.eulerAngles;

        euler.x = 0;
        euler.z = 0;

        return Quaternion.Euler(euler);
    }

    void HandleRotation()
    {
        Vector3 screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 10);
        var aim = _camera.ScreenToWorldPoint(screenCenter);

        // Tower Rotation
        var towerDirection = aim - transform.position;
        towerDirection.y = 0; // We only want the tower to rotate on the Y-axis.

        var targetRotation = Quaternion.LookRotation(towerDirection);

        tower.rotation = RemoveXAndZAxis(targetRotation) * _towerOffset * Quaternion.Euler(towerRotationOffset);


        // Barrel Rotations
        RotateTowards(barrel1, aim);
        RotateTowards(barrel2, aim);
    }


    void RotateTowards(Transform barrel, Vector3 target)
    {
        var direction = target - barrel.position;
        Quaternion desiredRotation = Quaternion.LookRotation(direction);

        // Extract the X rotation from our desired rotation and apply it to the barrel
        float desiredXRotation = desiredRotation.eulerAngles.x;
        var targetRotation = Quaternion.Euler(desiredXRotation, 0, 0);
        barrel.localRotation = Quaternion.Lerp(barrel.localRotation, targetRotation, Time.deltaTime * lerpSpeed);
    }

    // ReSharper disable Unity.PerformanceAnalysis
    [ServerRpc(RequireOwnership = false)]
    private void ShootServerRpc(Vector3 aim, bool isSpecialBullet, ServerRpcParams serverRpcParams = default)
    {
        Debug.Log("ShootServerRpc");
        var barrel = _isLeftGun ? barrel1 : barrel2;
        barrel.localScale = new Vector3(1, shootScale, 1);
        var rotation = barrel.rotation * Quaternion.Euler(bulletRotationOffset);
        var position = barrel.position + rotation * Vector3.forward * bulletTranslationOffset;

        var bulletRotation = Quaternion.LookRotation(aim - position);
        _isLeftGun = !_isLeftGun;

        float randomPitch = Random.Range(-randomRotation, randomRotation);
        float randomYaw = Random.Range(-randomRotation, randomRotation);
        bulletRotation *= Quaternion.Euler(randomPitch, randomYaw, 0);

        if (!isSpecialBullet)
        {
            var bulletNO = NetworkObjectPool.Singleton.GetNetworkObject(bullet, position, bulletRotation);
            bulletNO.SpawnWithOwnership(serverRpcParams!.Receive.SenderClientId);
            bulletNO.GetComponent<Bullet>().bulletPrefab = bullet;
        }
        else
        {
            var instBullet = Instantiate(isSpecialBullet ? specialBullet : bullet, position, bulletRotation);
            instBullet.GetComponent<NetworkObject>().SpawnWithOwnership(serverRpcParams!.Receive.SenderClientId);
        }

        PlayShootSoundClientRpc(serverRpcParams.Receive.SenderClientId, _isLeftGun);
    }

    [ClientRpc]
    private void PlayShootSoundClientRpc(ulong clientId, bool isLeft)
    {
        _isLeftGun = isLeft;
        if (clientId != PlayerController.LocalPlayer.OwnerClientId)
        {
            if (isLeft)
                audio1.Play();
            else
                audio2.Play();
        }
    }
}