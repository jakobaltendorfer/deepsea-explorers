using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Security.Cryptography;
using Networking;
using Submarine;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

public class Bullet : NetworkBehaviour
{
    [SerializeField] private float speed = 20f;
    [SerializeField] private float destroyTime = 10f;
    [SerializeField] private float activateColliderTime = 0.1f;
    [SerializeField] private int damage = 20;
    [SerializeField] private GameObject impact;
    [SerializeField] private bool specialBullet;

    private bool _colliderActivated;

    private float _timeTillDie;
    private float _timeTillCollider;

    [HideInInspector] public GameObject bulletPrefab; // reference

    private void OnEnable()
    {
        _timeTillDie = destroyTime;
        _timeTillCollider = activateColliderTime;
        GetComponent<Collider>().enabled = false;
        _colliderActivated = false;

        Invoke(nameof(EnableLineRenderer), 0.1f);
    }

    private void EnableLineRenderer()
    {
        GetComponentInChildren<TrailRenderer>().enabled = true;
    }

    private void OnDisable()
    {
        GetComponentInChildren<TrailRenderer>().enabled = false;
    }

    void Update()
    {
        if (!IsOwner) return;

        transform.position += Time.deltaTime * speed * transform.forward;
        _timeTillDie -= Time.deltaTime;
        _timeTillCollider -= Time.deltaTime;

        if (_timeTillDie <= 0)
            KillServerRpc(false);

        if (!_colliderActivated && _timeTillCollider <= 0)
        {
            GetComponent<Collider>().enabled = true;
            _colliderActivated = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!IsOwner) return;

        if (other.CompareTag("Bullet") || other.CompareTag("SpecialBullet")) return;

        if (specialBullet)
        {
            var intensity = CameraShaker.Instance.IsInDistance(transform.position);
            if (intensity > 0f)
            {
                CameraShaker.Instance.ShakeCamera(5f * intensity, 1.3f);
            }
        }

        Debug.Log(other.gameObject.name);
        var oilDerrick = other.GetComponentInParent<OilDerrick>();
        if (oilDerrick != null)
        {
            oilDerrick.TakeDamage(damage);
        }

        if (other.CompareTag("Enemy"))
        {
            Debug.Log("Hit an enemy");
            other.GetComponentInParent<EnemySubmarine>().TakeDamage(damage);
        }

        var samTurret = other.GetComponentInParent<SamTurret>();
        if (samTurret != null)
        {
            samTurret.TakeDamage(damage);
        }

        var whale = other.GetComponentInParent<Whale>();
        if (whale != null)
        {
            whale.TakeDamageServerRpc();
        }

        KillServerRpc(!specialBullet);
    }

    [ServerRpc(RequireOwnership = false)]
    private void KillServerRpc(bool showImpact)
    {
        if (showImpact)
        {
            GameObject impactGo = Instantiate(impact, transform.position, Quaternion.identity);
            impactGo.GetComponent<NetworkObject>().Spawn();
            impactGo.transform.localScale = Vector3.one * 0.25f;
            Destroy(impactGo, 1f);
        }

        if (!specialBullet)
        {
            var networkObject = GetComponent<NetworkObject>();
            networkObject.Despawn();
        }
        else
        {
            Destroy(gameObject);
        }
    }
}