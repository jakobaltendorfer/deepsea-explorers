using System;
using System.Collections;
using System.Collections.Generic;
using Networking;
using Unity.Netcode;
using UnityEngine;

public class Door : NetworkBehaviour
{
    [SerializeField] private float distance = 3f;
    [SerializeField] private float minDistance = 1.5f;
    [SerializeField] private float directionThreshold = 0.0f;

    private Animator _animator;
    private bool _isOpen;

    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (IsHost)
            CheckDoor();
    }

    void CheckDoor()
    {
        foreach (var client in NetworkManager.Singleton.ConnectedClientsList)
        {
            if (client.PlayerObject == null) continue;

            var player = client.PlayerObject.transform;
            float doorDistance = Vector3.Distance(transform.position, player.position);
            if (doorDistance < minDistance)
            {
                OpenDoor();
                return;
            }

            if (doorDistance < distance)
            {
                // Calculate the direction from the player to the door
                Vector3 directionToDoor = (transform.position - player.position).normalized;

                // Calculate the dot product
                float dotProduct = Vector3.Dot(player.forward, directionToDoor);

                // Check if player is facing the door (dotProduct close to 1)
                if (dotProduct > directionThreshold)
                {
                    OpenDoor();
                    return;
                }
            }
        }

        CloseDoor();
    }

    void OpenDoor()
    {
        if (!_isOpen)
        {
            _isOpen = true;
            OpenDoorClientRpc();
        }
    }

    void CloseDoor()
    {
        if (_isOpen)
        {
            _isOpen = false;

            CloseDoorClientRpc();
        }
    }

    [ClientRpc]
    void OpenDoorClientRpc()
    {
        _animator.Play("Door Open", 0, 0);
    }


    [ClientRpc]
    void CloseDoorClientRpc()
    {
        _animator.Play("Door Close", 0, 0);
    }
}