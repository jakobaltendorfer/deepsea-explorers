using Submarine;
using Unity.Netcode;
using UnityEngine;

public class Repair : NetworkBehaviour, IInteractable
{
    [SerializeField] private float repairCooldown = 1f;
    [SerializeField] private int healthPerHit = 10;
    [SerializeField] private AudioSource engineSound;

    private AudioSource _audio;
    private NetworkVariable<float> _repairTime = new NetworkVariable<float>();

    private void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    public void Interact(PlayerController playerController)
    {
        if (_repairTime.Value > 0f) return;

        if (SubmarineController.Instance.CanRepair())
            DoRepair();
    }

    private void DoRepair()
    {
        DoRepairServerRpc();
    }

    private void Update()
    {
        if (IsHost)
            _repairTime.Value -= Time.deltaTime;
    }

    [ServerRpc(RequireOwnership = false)]
    private void DoRepairServerRpc()
    {
        DialogueManager.Instance.RemoveMissionServerRpc("repair");
        SubmarineController.Instance.ChangeHealth(healthPerHit);
        _repairTime.Value = repairCooldown;
        PlayRepairSoundClientRpc();
    }

    [ClientRpc]
    private void PlayRepairSoundClientRpc()
    {
        _audio.Play();
    }

    public void StartEngine(bool active)
    {
        if (active)
            engineSound.Play();
        else
            engineSound.Stop();
    }
}