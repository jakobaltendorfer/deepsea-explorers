using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitcher : MonoBehaviour
{
    [SerializeField] private float sleepTime = 0.5f;

    private readonly List<GameObject> _lights = new List<GameObject>();
    private bool _lightsActive = false, _switching;

    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var light = transform.GetChild(i).gameObject;
            light.SetActive(false);
            _lights.Add(light);
        }
    }

    public void StartSwitchLights(bool active)
    {
        Debug.Log("StartSwitchLights");

        if (!_switching)
        {
            if (_lightsActive != active)
                StartCoroutine(SwitchLights(active));
        }
    }

    IEnumerator SwitchLights(bool active)
    {
        Debug.Log("SwitchLights StartCoroutine");

        _switching = true;
        foreach (var light in _lights)
        {
            light.SetActive(active);

            yield return new WaitForSeconds(sleepTime);
        }

        _lightsActive = active;
        _switching = false;
    }
}