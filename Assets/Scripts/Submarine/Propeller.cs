using UnityEngine;
using UnityEngine.VFX;

namespace Submarine
{
    public class Propeller : MonoBehaviour
    {
        private Rigidbody _rb;
        private VisualEffect _visualEffect;

        // Start is called before the first frame update
        void Start()
        {
            _rb = SubmarineController.Instance.transform.GetComponent<Rigidbody>();
            _visualEffect = GetComponent<VisualEffect>();
        }

        // Update is called once per frame
        void Update()
        {
            _visualEffect.SetVector3("Velocity", _rb.velocity);
        }
    }
}